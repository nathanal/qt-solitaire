#include <QApplication>
#include <QPluginLoader>
#include "deckui/gamemanager.h"
#include <QScreen>
#include <QSize>

Q_IMPORT_PLUGIN(PyramidsNeoUiDelegate)

int main(int argc, char **argv)
{
	QApplication app(argc, argv);
	Q_INIT_RESOURCE(resources);

	QSize dim = app.primaryScreen()->availableVirtualSize()*0.8;

	GameManager m(dim.width(), dim.height());
	m.show();
	
	return app.exec();
}