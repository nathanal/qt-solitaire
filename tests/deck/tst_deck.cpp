#include <QtTest>
#include <algorithm>
#include "deck/card.h"
#include "deck/deck.h"

class tst_Deck : public QObject
{
	Q_OBJECT
	Deck *deck;
	bool validDeck();
	bool sameDeck(Deck&, Deck&);
private slots:
	void init();
	void cleanup();
	void validCards();
	void cardPrinting(); // only validates that each output is unique, not that they correspond to the correct id
	void cardIdentification();
	void cardIdentification_data();
	void cardComparison();
	void cardComparison_data();
	void validShuffling();
	void validSaving();
};

void tst_Deck::init(){ deck = new Deck(); }
void tst_Deck::cleanup(){ delete deck; deck = nullptr; }

void tst_Deck::validCards()
{
	QVERIFY(!Deck::invalidCard.isValid()); // check invalidCard is initialised to invalid
	// check that Card::id() will correspond 
	// to a single Card::Suit and Card::Number
	QVector<QVector<int>> cards;
	cards.resize(Card::suitsInDeck);
	for (int i=0; i<Card::suitsInDeck; i++){
		cards[i].resize(Card::cardsInDeck);
		std::fill(cards[i].begin(), cards[i].end(), 0);
	}

	int nCards = deck->size();
	int target = nCards / 52;
	target *= 2; // each card is added twice
	QCOMPARE(nCards % 52, 0);

	for (int i=0; i<nCards; i++){
		Card &cur = (*deck)[i];
		int indexA = static_cast<int>(cur.suit());
		int indexB = static_cast<int>(cur.number());
		QVERIFY2((indexA < Card::suitsInDeck) && (indexA) >= 0,
			"Test function works as expected");
		QVERIFY2((indexB < Card::cardsInDeck) && (indexB >= 0), 
			"Test function works as expected");

		cards[indexA][indexB] ++;
		cards[indexA][cur.value()-1] ++;
	}
	for (int i=0; i<Card::suitsInDeck; i++){
		for (int j=0; j<Card::cardsInSuit; j++){
			QCOMPARE(cards[i][j], target);
		}
	}
}

void tst_Deck::cardPrinting()
{
	delete deck;
	deck = new Deck(1); // just make sure its a single deck
	QString unicodes = Deck::invalidCard.toUnicode() + " ";
	QString acrUnicodes = Deck::invalidCard.toUnicode(true) + " ";
	QString acrs = Deck::invalidCard.toString(true) + " ";
	QString writtenCards = Deck::invalidCard.toString() + " ";
	for (int i=0; i<deck->size(); i++){
		QString unicode = (*deck)[i].toUnicode();
		QVERIFY(!unicodes.contains(unicode));
		unicodes += unicode + " ";
		QVERIFY2(unicode.size() == 2, "Unicode card characters takes 2 QChar");

		unicode = (*deck)[i].toUnicode(true);
		QVERIFY(!acrUnicodes.contains(unicode));
		acrUnicodes += unicode + " ";
		QVERIFY2(unicode.size() == 2, "Unicode card characters takes 2 QChar");

		QString acr = (*deck)[i].toString(true);
		QVERIFY(!acrs.contains(acr));
		acrs +=  acr + " ";

		QString writtenCard = (*deck)[i].toString();
		QVERIFY(!writtenCards.contains(writtenCard));
		writtenCards += writtenCard + " ";
	}
}

void tst_Deck::cardIdentification_data()
{
    QTest::addColumn<int>("number");
    QTest::addColumn<int>("suit");
    QTest::addColumn<bool>("isRed");
    QTest::addColumn<bool>("isFace");

    QTest::newRow("Ace of Hearts") << static_cast<int>(Card::Number::ACE)
    								  << static_cast<int>(Card::Suit::HEARTS)
    								  << true << false;
    QTest::newRow("Ace of Clubs") << static_cast<int>(Card::Number::ACE)
    								  << static_cast<int>(Card::Suit::CLUBS)
    								  << false << false;
    QTest::newRow("Five of Spades") << static_cast<int>(Card::Number::FIVE)
    								  << static_cast<int>(Card::Suit::SPADES)
    								  << false << false;
    QTest::newRow("Seven of diamonds") << static_cast<int>(Card::Number::SEVEN)
    								  << static_cast<int>(Card::Suit::DIAMONDS)
    								  << true << false;
    QTest::newRow("Ten of Spades") << static_cast<int>(Card::Number::TEN)
    								  << static_cast<int>(Card::Suit::SPADES)
    								  << false << false;
    QTest::newRow("Jack of Diamonds") << static_cast<int>(Card::Number::JACK)
    								  << static_cast<int>(Card::Suit::DIAMONDS)
    								  << true << true;
    QTest::newRow("King of Spades") << static_cast<int>(Card::Number::KING)
    								  << static_cast<int>(Card::Suit::SPADES)
    								  << false << true;
}

void tst_Deck::cardIdentification()
{
    QFETCH(int, number);
    QFETCH(int, suit);
    Card card(static_cast<Card::Number>(number), static_cast<Card::Suit>(suit));
    QTEST(card.isRed(), "isRed");
    QTEST(!card.isBlack(), "isRed");
    QTEST(card.isFaceCard(), "isFace");
    QVERIFY(card==card);
    QVERIFY(card.same(card));
    QVERIFY(card>=card);
    QVERIFY(card<=card);
    QVERIFY(!(card<card));
    QVERIFY(!(card>card));
}

void tst_Deck::cardComparison_data()
{
    QTest::addColumn<int>("numberA");
    QTest::addColumn<int>("suitA");

    QTest::addColumn<int>("numberB");
    QTest::addColumn<int>("suitB");

    QTest::addColumn<bool>("adjNumber"); // note King and Ace aren't adjacent by default
    QTest::addColumn<bool>("adjSuit"); // note Spades and Diamonds aren't adjacent by default

    QTest::addColumn<bool>("sameSuit");
    QTest::addColumn<bool>("sameColour");
    QTest::addColumn<bool>("sameNumber");

    QTest::addColumn<bool>("same");
    QTest::addColumn<bool>("greater");

    QTest::newRow("Same") 			  << static_cast<int>(Card::Number::ACE)
    								  << static_cast<int>(Card::Suit::HEARTS)
    								  << static_cast<int>(Card::Number::ACE)
    								  << static_cast<int>(Card::Suit::HEARTS)
    								  << false << false
    								  << true << true << true
    								  << true << false;
    QTest::newRow("Differing Number") << static_cast<int>(Card::Number::QUEEN)
    								  << static_cast<int>(Card::Suit::HEARTS)
    								  << static_cast<int>(Card::Number::ACE)
    								  << static_cast<int>(Card::Suit::HEARTS)
    								  << false << false
    								  << true << true << false
    								  << false << true;
    QTest::newRow("Differing Suit") << static_cast<int>(Card::Number::TEN)
    								  << static_cast<int>(Card::Suit::DIAMONDS)
    								  << static_cast<int>(Card::Number::TEN)
    								  << static_cast<int>(Card::Suit::HEARTS)
    								  << false << false
    								  << false << true << true
    								  << false << false;
    QTest::newRow("Differing Colour") << static_cast<int>(Card::Number::JACK)
    								  << static_cast<int>(Card::Suit::DIAMONDS)
    								  << static_cast<int>(Card::Number::JACK)
    								  << static_cast<int>(Card::Suit::CLUBS)
    								  << false << true
    								  << false << false << true
    								  << false << false;
    QTest::newRow("Same colour") << static_cast<int>(Card::Number::FIVE)
    								  << static_cast<int>(Card::Suit::SPADES)
    								  << static_cast<int>(Card::Number::TEN)
    								  << static_cast<int>(Card::Suit::CLUBS)
    								  << false << false
    								  << false << true << false
    								  << false << false;
    QTest::newRow("Differing Card/Adjacent") << static_cast<int>(Card::Number::QUEEN)
    								  << static_cast<int>(Card::Suit::HEARTS)
    								  << static_cast<int>(Card::Number::JACK)
    								  << static_cast<int>(Card::Suit::SPADES)
    								  << true << true
    								  << false << false << false
    								  << false << true;

    QTest::newRow("Adjacent Number") << static_cast<int>(Card::Number::TEN)
    								  << static_cast<int>(Card::Suit::DIAMONDS)
    								  << static_cast<int>(Card::Number::JACK)
    								  << static_cast<int>(Card::Suit::SPADES)
    								  << true << false
    								  << false << false << false
    								  << false << false;
    QTest::newRow("Adjacent Suit") << static_cast<int>(Card::Number::ACE)
    								  << static_cast<int>(Card::Suit::DIAMONDS)
    								  << static_cast<int>(Card::Number::THREE)
    								  << static_cast<int>(Card::Suit::CLUBS)
    								  << false << true
    								  << false << false << false
    								  << false << false;
    QTest::newRow("Non-Adjacent") << static_cast<int>(Card::Number::ACE)
    								  << static_cast<int>(Card::Suit::SPADES)
    								  << static_cast<int>(Card::Number::THREE)
    								  << static_cast<int>(Card::Suit::CLUBS)
    								  << false << false
    								  << false << true << false
    								  << false << false;
}

void tst_Deck::cardComparison()
{
    QFETCH(int, numberA);
    QFETCH(int, suitA);
    QFETCH(int, numberB);
    QFETCH(int, suitB);
    Card cardA = Card(static_cast<Card::Number>(numberA), static_cast<Card::Suit>(suitA));
    Card cardB = Card(static_cast<Card::Number>(numberB), static_cast<Card::Suit>(suitB));

	QTEST(cardA.adjNumber(cardB), "adjNumber");
	QTEST(cardA.adjSuit(cardB) , "adjSuit");
	QTEST(cardA.sameSuit(cardB) , "sameSuit");
	QTEST(cardA.sameColour(cardB) , "sameColour");
	QTEST(cardA.sameNumber(cardB) , "sameNumber");
	QFETCH(bool, same);
	QFETCH(bool, greater);
	bool lesser = (!same) && (!greater);
	QCOMPARE(cardA==cardB, same);
	QCOMPARE(cardA.same(cardB), same);
	QCOMPARE(cardA>cardB, greater);
	QCOMPARE(cardA>=cardB, greater || same);
	QCOMPARE(cardA<cardB, lesser);
	QCOMPARE(cardA<=cardB, lesser || same);
}

bool tst_Deck::validDeck()
{
	int nCards = deck->size();
	if ((nCards % Card::cardsInDeck) != 0){ return false; }
	int nDecks = nCards/Card::cardsInDeck;

	int *counters = new int[Card::cardsInDeck];
	for (int i=0; i<Card::cardsInDeck; i++){
		counters[i] = 0;
	}
	for (int i=0; i<nCards; i++){
		if ((++counters[(*deck)[i].id()]) > nDecks){
			return false;
		}
	}
	return true;
}

bool tst_Deck::sameDeck(Deck& a, Deck& b)
{
	int size = a.size();
	if (size != b.size()){ return false; }
	for (int i=0; i<size; i++){
		if (a[i].id() != b[i].id()){ return false; }
	}
	return true;
}

void tst_Deck::validShuffling()
{
	QVERIFY2(validDeck(), "Initial deck failed.");
	deck->shuffle();
	QVERIFY2(validDeck(), "Shuffled deck failed.");
	deck->shuffle(11111);
	QVERIFY2(validDeck(), "Shuffled deck (seed=11111) failed.");
	deck->shuffle(true);
	QVERIFY2(validDeck(), "Shuffled deck (continueGame) failed.");

	delete deck;
	deck = new Deck(5);

	QVERIFY2(validDeck(), "Initial decks (5) failed.");
	deck->shuffle();
	QVERIFY2(validDeck(), "Shuffled decks (5) failed.");
	deck->shuffle(11111);
	QVERIFY2(validDeck(), "Shuffled decks (5) (seed=11111) failed.");
	deck->shuffle(true);
	QVERIFY2(validDeck(), "Shuffled decks (5) (continueGame) failed.");
}

void tst_Deck::validSaving()
{
	Deck oldDeck(*deck);
	QVERIFY2(validDeck(), "Initial deck failed.");
	QString oldDeckData = deck->save();
	deck->shuffle(11112);
	Deck newDeck(*deck);
	QString newDeckData = deck->save();
	QVERIFY2(validDeck(), "Shuffled deck failed.");

	deck->load(oldDeckData);
	QVERIFY2(!deck->load(oldDeckData.left(20)), "Invalid save string loaded somehow");
	QVERIFY2(sameDeck(oldDeck, *deck), "Failed save of unshuffled deck");
	QVERIFY2(oldDeck.save() == deck->save() && oldDeck.save() == oldDeckData, "Same deck produces different save files");

	deck->load(newDeckData);
	QVERIFY2(sameDeck(newDeck, *deck), "Failed save of shuffled deck");
	QVERIFY2(newDeck.save() == deck->save() && newDeck.save() == newDeckData, "Same deck produces different save files");
}

QTEST_APPLESS_MAIN(tst_Deck)
#include "tst_deck.moc"