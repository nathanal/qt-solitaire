#include <QtTest>
#include <QObject>
#include "pyramids/pyramidsneo.h"

class tst_PyramidsNeo : public QObject
{
	Q_OBJECT
	IGame *game;
	const int initialisingSeed = 15;
	bool validMoves();
private slots:
	void initTestCase();
	void init();
	void saveLoadOp();
	void testPiles();
	void testCardsThere();
	void testCardsVisible();
	void testCardsMovable();
	void testMoves();
};

void tst_PyramidsNeo::initTestCase() { game = new PyramidsNeo(initialisingSeed); }
void tst_PyramidsNeo::init() { game->restartGame(); }

bool tst_PyramidsNeo::validMoves()
{
	return (game->move(IGame::Pile::LAYOUT, IGame::Pile::WASTE, 24, -1)
			&& game->move(IGame::Pile::LAYOUT, IGame::Pile::WASTE, 25, -1)
			&& game->move(IGame::Pile::STOCK, IGame::Pile::WASTE, 0, 0)
			&& game->move(IGame::Pile::LAYOUT, IGame::Pile::WASTE, 23, -1)
		);
}

void tst_PyramidsNeo::saveLoadOp()
{
	QString prev = game->save();
	QVERIFY2(validMoves(), "predetermined valid moves failing");
	QString cur = game->save();
	game->restartGame();
	QCOMPARE(game->save(), prev);
	QVERIFY2(game->load(cur), "Load game with moves done to it");
	QVERIFY2(game->load(prev), "Load blank game");
}

void tst_PyramidsNeo::testPiles()
{
	QVERIFY(game->pileExists(IGame::Pile::STOCK));
	QVERIFY(game->pileExists(IGame::Pile::WASTE));
	QVERIFY(game->pileExists(IGame::Pile::LAYOUT));
	QVERIFY(!game->pileExists(IGame::Pile::CELLS));
	QVERIFY(!game->pileExists(IGame::Pile::RESERVE));
	QVERIFY(!game->pileExists(IGame::Pile::FOUNDATION));

	QCOMPARE(game->size(), 52);
	QCOMPARE(game->pileInitSize(IGame::Pile::STOCK), 23);
	QCOMPARE(game->pileInitSize(IGame::Pile::WASTE), 1);
	QCOMPARE(game->pileInitSize(IGame::Pile::LAYOUT), 28);
	QCOMPARE(game->pileInitSize(IGame::Pile::CELLS), 0);
	QCOMPARE(game->pileInitSize(IGame::Pile::RESERVE), 0);
	QCOMPARE(game->pileInitSize(IGame::Pile::FOUNDATION), 0);

	QCOMPARE(game->pileInitSize(IGame::Pile::STOCK), game->pileSize(IGame::Pile::STOCK));
	QCOMPARE(game->pileInitSize(IGame::Pile::WASTE), game->pileSize(IGame::Pile::WASTE));
	QCOMPARE(game->pileInitSize(IGame::Pile::LAYOUT), game->pileSize(IGame::Pile::LAYOUT));
	QCOMPARE(game->pileInitSize(IGame::Pile::CELLS), game->pileSize(IGame::Pile::CELLS));
	QCOMPARE(game->pileInitSize(IGame::Pile::RESERVE), game->pileSize(IGame::Pile::RESERVE));
	QCOMPARE(game->pileInitSize(IGame::Pile::FOUNDATION), game->pileSize(IGame::Pile::FOUNDATION));
}

void tst_PyramidsNeo::testCardsThere()
{
	for (int i=0; i<game->pileSize(IGame::Pile::LAYOUT); i++){
		QVERIFY(game->isCardThere(IGame::Pile::LAYOUT, i));
	}
	QVERIFY(!game->isCardThere(IGame::Pile::LAYOUT, game->pileSize(IGame::Pile::LAYOUT)));

	for (int i=0; i<game->pileSize(IGame::Pile::WASTE); i++){
		QVERIFY(game->isCardThere(IGame::Pile::WASTE, i));
	}
	QVERIFY(!game->isCardThere(IGame::Pile::WASTE, game->pileSize(IGame::Pile::WASTE)));

	for (int i=0; i<game->pileSize(IGame::Pile::STOCK); i++){
		QVERIFY(game->isCardThere(IGame::Pile::STOCK, i));
	}
	QVERIFY(!game->isCardThere(IGame::Pile::STOCK, game->pileSize(IGame::Pile::STOCK)));

	for (int i=0; i<game->pileSize(IGame::Pile::CELLS); i++){
		QVERIFY(game->isCardThere(IGame::Pile::CELLS, i));
	}
	QVERIFY(!game->isCardThere(IGame::Pile::CELLS, game->pileSize(IGame::Pile::CELLS)));

	for (int i=0; i<game->pileSize(IGame::Pile::RESERVE); i++){
		QVERIFY(game->isCardThere(IGame::Pile::RESERVE, i));
	}
	QVERIFY(!game->isCardThere(IGame::Pile::RESERVE, game->pileSize(IGame::Pile::RESERVE)));

	for (int i=0; i<game->pileSize(IGame::Pile::FOUNDATION); i++){
		QVERIFY(game->isCardThere(IGame::Pile::FOUNDATION, i));
	}
	QVERIFY(!game->isCardThere(IGame::Pile::FOUNDATION, game->pileSize(IGame::Pile::FOUNDATION)));
}

void tst_PyramidsNeo::testCardsVisible()
{
	for (int i=0; i<game->pileSize(IGame::Pile::LAYOUT); i++){
		if (game->isCardVisible(IGame::Pile::LAYOUT, i)){
			QVERIFY2(i > 20, "Last layer visible");
		} else {
			QVERIFY2(i <= 20, "Card is not in last layer");
		}
	}
	for (int i=0; i<game->pileSize(IGame::Pile::WASTE); i++){
		if (game->isCardVisible(IGame::Pile::WASTE, i)){
			QVERIFY2(i+1 == game->pileSize(IGame::Pile::WASTE), "Top of waste pile");
		} else {
			QVERIFY2(i+1 != game->pileSize(IGame::Pile::WASTE), "Not on top of waste pile");
		}
	}

	for (int i=0; i<game->pileSize(IGame::Pile::STOCK); i++){
		QVERIFY2(!game->isCardVisible(IGame::Pile::STOCK, i), "Cards in stock not visible");
	}
}

void tst_PyramidsNeo::testCardsMovable()
{
	QVERIFY(!game->isCardMovable(IGame::Pile::WASTE, 0));
	QVERIFY(game->isCardMovable(IGame::Pile::STOCK, 0));

	int movableLayoutCard = 24;
	for (int i=0; i<game->pileSize(IGame::Pile::LAYOUT); i++){
		if (i != movableLayoutCard){
			QVERIFY(!game->isCardMovable(IGame::Pile::LAYOUT, i));
		} else {
			QVERIFY(game->isCardMovable(IGame::Pile::LAYOUT, i));
		}
	}
	game->move(IGame::Pile::LAYOUT, IGame::Pile::WASTE, movableLayoutCard, -1);
	QVERIFY(!game->isCardThere(IGame::Pile::LAYOUT, movableLayoutCard)); // since it is gone
	QVERIFY(!game->isCardMovable(IGame::Pile::LAYOUT, movableLayoutCard)); // since it is gone
	QVERIFY(game->isCardMovable(IGame::Pile::LAYOUT, movableLayoutCard+1)); // this card should now be movable

	QVERIFY(!game->isCardMovable(IGame::Pile::WASTE, 0)); // check, now that the card has changed
	while (game->pileSize(IGame::Pile::STOCK) != 0){
		game->move(IGame::Pile::STOCK, IGame::Pile::WASTE, -1, -1);
	}
	QVERIFY(!game->isCardThere(IGame::Pile::STOCK, 0));
	QVERIFY(!game->isCardMovable(IGame::Pile::STOCK, 0));
	QVERIFY(!game->isCardMovable(IGame::Pile::WASTE, 0)); // double check, now that the card has changed
}

void tst_PyramidsNeo::testMoves()
{
	QVERIFY2(game->move(IGame::Pile::LAYOUT, IGame::Pile::WASTE, 24, -1)
		, "Remove card");
	QVERIFY2(game->move(IGame::Pile::STOCK, IGame::Pile::WASTE, 0, 0)
		, "Deal");
	QVERIFY2(!game->move(IGame::Pile::LAYOUT, IGame::Pile::WASTE, 25, -1)
		, "Can't move this card anymore");
	QVERIFY2(!game->move(IGame::Pile::LAYOUT, IGame::Pile::WASTE, 10, -1)
		, "Can't move this card yet");
	QVERIFY2(!game->move(IGame::Pile::WASTE, IGame::Pile::STOCK, 0, 0)
		, "Can't move cards out of waste");
	QVERIFY(!game->isGameOver());
	while (game->pileSize(IGame::Pile::STOCK) != 0){
		QVERIFY2(game->move(IGame::Pile::STOCK, IGame::Pile::WASTE, -1, -1), "Deal");
	}
	QVERIFY2(game->move(IGame::Pile::LAYOUT, IGame::Pile::WASTE, 22, -1)
		, "Remove card");
	QVERIFY2(game->move(IGame::Pile::LAYOUT, IGame::Pile::WASTE, 21, -1)
		, "Remove card");
	
	QVERIFY(game->isGameOver());
	QVERIFY(!game->isWin());
}

QTEST_APPLESS_MAIN(tst_PyramidsNeo)
#include "tst_pyramidsneo.moc"