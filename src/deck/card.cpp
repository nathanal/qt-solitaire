#include "deck/card.h"

Card::Card(int id)
	: m_id(id)
{
	if (!isValid(id)){
		m_id = invalid;
	}
	fillFromID();
}

Card::Card(Card::Number num, Card::Suit suit)
	: m_suit(suit)
	, m_number(num)
{
	if (num == Number::INVALID || suit == Suit::INVALID){
		m_id = invalid;
		m_suit = Suit::INVALID;
		m_number = Number::INVALID;
		m_value = invalid;
	} else {
		for (int i=0; i<cardsInDeck; i++){
			Card buff = Card(i);
			if ((buff.number() == num) && (buff.suit() == suit)){
				this->operator=(buff);
				break;
			}
		}
	}
}

Card& Card::operator=(const Card &orig)
{
	m_id = orig.m_id;
	m_suit = orig.m_suit;
	m_number = orig.m_number;
	m_value = orig.m_value;
	return *this;
}

void Card::fillFromID()
{
	if (m_id == invalid){
		m_suit = Suit::INVALID;
		m_number = Number::INVALID;
		m_value = invalid;
	} else {
		m_value = m_id/suitsInDeck;
		switch (m_value){
		case 0:
			m_number = Number::ACE;
			break;
		case 1:
			m_number = Number::TWO;
			break;
		case 2:
			m_number = Number::THREE;
			break;
		case 3:
			m_number = Number::FOUR;
			break;
		case 4:
			m_number = Number::FIVE;
			break;
		case 5:
			m_number = Number::SIX;
			break;
		case 6:
			m_number = Number::SEVEN;
			break;
		case 7:
			m_number = Number::EIGHT;
			break;
		case 8:
			m_number = Number::NINE;
			break;
		case 9:
			m_number = Number::TEN;
			break;
		case 10:
			m_number = Number::JACK;
			break;
		case 11:
			m_number = Number::QUEEN;
			break;
		case 12:
			m_number = Number::KING;
			break;
		default:
			m_value = invalid;
			m_number = Number::INVALID;
			break;
		}
		if (m_value != invalid){ m_value++; }

		switch (m_id%suitsInDeck){
		case 0:
			m_suit = Suit::DIAMONDS;
			break;
		case 1:
			m_suit = Suit::CLUBS;
			break;
		case 2:
			m_suit = Suit::HEARTS;
			break;
		case 3:
			m_suit = Suit::SPADES;
			break;
		default:
			m_suit = Suit::INVALID;
			break;
		}
	}		
}

void Card::swap(Card& a)
{
	std::swap(m_id, a.m_id);
	std::swap(m_suit, a.m_suit);
	std::swap(m_number, a.m_number);
	std::swap(m_value, a.m_value);
}

void swap(Card &a, Card &b)
{
	a.swap(b);
}

bool Card::isValid(int i) const { return (i >= 0) && (i < cardsInDeck); }
bool Card::isValid() const { return isValid(m_id); }

int Card::id() const { return m_id; }
int Card::value() const { return m_value; }
Card::Number Card::number() const { return m_number; }
Card::Suit Card::suit() const { return m_suit; }

bool Card::isRed() const { return (m_suit == Suit::DIAMONDS) || (m_suit == Suit::HEARTS); }
bool Card::isBlack() const { return !isRed(); }
bool Card::isFaceCard() const { return (m_number == Number::JACK) || (m_number == Number::QUEEN) || (m_number == Number::KING); }
bool Card::sameSuit(const Card &c) const { return m_suit == c.m_suit; }
bool Card::sameColour(const Card &c) const { return (isRed() == c.isRed()); }
bool Card::sameNumber(const Card &c) const { return m_number == c.m_number; }
bool Card::adjNumber(const Card& c, bool wrap) const
{
	int n = m_value;
	int m = c.m_value;
	if (n == invalid || m == invalid){ return false; }
	if (n > m){ std::swap(n, m); }
	if (wrap && n == 1 && m == cardsInSuit){ return true; }
	return ((n+1) == m);
}
bool Card::adjSuit(const Card &c, bool wrap) const
{
	if (m_suit == Suit::INVALID || c.m_suit == Suit::INVALID){ return false; }
	Suit s = m_suit;
	Suit t = c.m_suit;
	if (s > t){ std::swap(s, t); }
	if (wrap && s == Suit::DIAMONDS && t == Suit::SPADES){ return true; }
	return (static_cast<int>(s)+1 == static_cast<int>(t));
}
bool Card::same(const Card &c) const { return *this == c; }
bool Card::operator==(const Card &c) const { return m_id == c.m_id; }
bool Card::operator>(const Card &c) const { return m_id > c.m_id; }
bool Card::operator>=(const Card &c) const { return m_id >= c.m_id; }
bool Card::operator<(const Card &c) const { return m_id < c.m_id; }
bool Card::operator<=(const Card &c) const { return m_id <= c.m_id; }

QString Card::toString(Suit s) const
{
	switch (s){
		case Suit::DIAMONDS:
			return "Diamonds";
		case Suit::CLUBS:
			return "Clubs";
		case Suit::HEARTS:
			return "Hearts";
		case Suit::SPADES:
			return "Spades";
		case Suit::INVALID:
		default:
			return "Invalid";
	}
}
QString Card::toString(Number n) const
{
	switch (n){
	case Number::ACE:
		return "Ace";
	case Number::TWO:
		return "Two";
	case Number::THREE:
		return "Three";
	case Number::FOUR:
		return "Four";
	case Number::FIVE:
		return "Five";
	case Number::SIX:
		return "Six";
	case Number::SEVEN:
		return "Seven";
	case Number::EIGHT:
		return "Eight";
	case Number::NINE:
		return "Nine";
	case Number::TEN:
		return "Ten";
	case Number::JACK:
		return "Jack";
	case Number::QUEEN:
		return "Queen";
	case Number::KING:
		return "King";
	case Number::INVALID:
	default:
		return "Invalid";
	}
}

QString Card::toString(bool acronym) const
{
	if (m_id == invalid){
		return (acronym?"..":"Face Down");
	}
	if (acronym){
		QString number;
		if(m_value == invalid){
			number.setNum(0);
		} else if (m_value < 10 && m_value > 1){
			number.setNum(m_value);
		} else {
			number = toString(m_number).left(1);
		}
		return (number + toString(m_suit).left(1));
	} else {
		return toString(m_number) + " of " + toString(m_suit);
	}
}

QString Card::toUnicode(bool acronym) const
{
	if (acronym){
		QString unicode;
		switch (m_number){
		case Number::ACE:
			unicode = "A";
			break;
		case Number::TEN:
			unicode = "T";
			break;
		case Number::JACK:
			unicode = "J";
			break;
		case Number::QUEEN:
			unicode = "Q";
			break;
		case Number::KING:
			unicode = "K";
			break;
		default:
			unicode = static_cast<char>('0' + m_value);
			break;
		}
		switch (m_suit){
		case Suit::DIAMONDS:
			unicode += "♦";
			break;
		case Suit::CLUBS:
			unicode += "♣";
			break;
		case Suit::HEARTS:
			unicode += "♥";
			break;
		case Suit::SPADES:
			unicode += "♠";
			break; 
		case Suit::INVALID:
		default:
			unicode = "..";
			break;
		}
		return unicode;
	} else {
		uint unicode = 0x1F0A0 + m_value;
		if (m_value > 11){ unicode ++; }
		switch (m_suit){
		case Suit::DIAMONDS:
			unicode += 0x20;
			break;
		case Suit::CLUBS:
			unicode += 0x30;
			break;
		case Suit::HEARTS:
			unicode += 0x10;
			break;
		case Suit::SPADES:
			break;
		case Suit::INVALID:
		default:
			unicode = 0x1F0A0;
			break;
		}
		return QString::fromUcs4(&unicode, 1);
	}
}