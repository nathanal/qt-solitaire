#include "deck/deck.h"

Card Deck::invalidCard = Card();

Deck::Deck(int nDecks)
{
	for (int i=0; i<Card::cardsInDeck; i++){
		for (int j=0; j<nDecks; j++){
			m_cards.push_back(Card(i));
		}
	}
}

Deck& Deck::operator=(const Deck& d)
{
	rand = d.rand;
	m_cards = d.m_cards;
	return *this;
}

bool Deck::load(QString savedFile)
{
	QByteArray data;
	data.append(savedFile);
	try {
		data = qUncompress(QByteArray::fromBase64(data));
	} catch ( ... ) {
		return false;
	}
	std::vector<Card> cards;
	std::vector<int> counter;
	counter.resize(Card::cardsInDeck);

	bool validFile = true;
	for (auto it = data.begin(); it < data.end(); it++){
		cards.push_back(Card(*it));
		if (!cards.back().isValid()){
			validFile = false;
			break;
		} else {
			counter[cards.back().id()]++;
		}
	}
	if (cards.size() == 0 || ((cards.size() % Card::cardsInDeck) != 0)){
		validFile = false;
	} else if (std::count(counter.begin(), counter.end(), cards.size() / Card::cardsInDeck) != Card::cardsInDeck){

		validFile = false;
	}
	// need error checking, make sure no duplicates/invalid cards
	if (validFile){
		m_cards = std::move(cards);
	}
	return validFile;
}

QString Deck::save() const
{
	QByteArray data;
	for (int i=0; i<static_cast<int>(m_cards.size()); i++){
		data.append(static_cast<char>(m_cards[i].id()));
	}
	return QString(qCompress(data).toBase64());
}


void Deck::shuffle(bool continueGame)
{
	if (!continueGame){
		rand.seed(QRandomGenerator::global()->generate());
	}
	for (int i=0; i<static_cast<int>(m_cards.size()); i++){
		int j = rand.bounded(static_cast<int>(m_cards.size()));
		std::swap(m_cards[i], m_cards[j]);
	}
}

void Deck::shuffle(int seed)
{
	rand.seed(seed);
	shuffle(true);
}

int Deck::size() const { return m_cards.size(); }


Card& Deck::at(int index)
{
	if (index < 0){ index = 0; }
	else if (index >= static_cast<int>(m_cards.size())){ index = static_cast<int>(m_cards.size())-1; }
	return m_cards[index];
}

const Card& Deck::at(int index) const
{
	if (index < 0){ index = 0; }
	else if (index >= static_cast<int>(m_cards.size())){ index = static_cast<int>(m_cards.size())-1; }
	return m_cards[index];
}

Card& Deck::operator[](int index)
{
	return at(index);
}

const Card& Deck::operator[](int index) const
{
	return at(index);
}