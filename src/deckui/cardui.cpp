#include "deckui/cardui.h"

CardUi::CardUi(Card *card, ImageManager *manager, QGraphicsItem *parent)
	: QGraphicsObject(parent)
	, m_faceUp(false)
	, m_card(card)
	, m_manager(manager)
	, m_status(Status::NO_MOVE)
	, m_selected(false)
	, m_bufferFlip(false)
	, m_bufferCard(nullptr)
{
	setAcceptHoverEvents(true);

	m_effect = new QGraphicsDropShadowEffect(this);
	m_effect->setOffset(0, 0);
	setGraphicsEffect(m_effect);
	updateEffects();

	connect(m_manager, &ImageManager::changedImage, this, &CardUi::updateImages);
	m_width = m_manager->width();
	m_height = m_manager->height();
	initAnimation();
	update();
}

void CardUi::updateEffects()
{
	m_effect->setBlurRadius(0.2*m_manager->width());
	switch (m_status){
	case Status::VALID_MOVE:
		m_effect->setColor(QColor(40, 255, 0, 255));
		break;
	case Status::INVALID_MOVE:
		m_effect->setColor(QColor(255, 20, 0, 255));
		break;
	case Status::NO_MOVE:
	default:
		if (m_selected){
			m_effect->setColor(QColor(200, 200, 0, 255));
		} else {
			m_effect->setBlurRadius(0.05*m_manager->width());
			m_effect->setColor(QColor(30, 30, 30, 255));
		}
	}
}

void CardUi::setStatus(Status status, bool isSelected)
{
	m_status = status;
	m_selected = isSelected;
	updateEffects();
	update();
}

IGame::Pile CardUi::pile() const { return m_pile; }
int CardUi::index() const { return m_index; }
void CardUi::setPile(IGame::Pile pile) { m_pile = pile; }
void CardUi::setIndex(int index) { m_index = index; } // fuck error checking (wouldn't be adequate anyway)

void CardUi::initAnimation()
{
	m_moveTransition = new QTimeLine(1, this);
	m_moveTransition->setFrameRange(transitionMin, transitionMax);
	m_moveTransition->setCurveShape(QTimeLine::EaseInCurve);
	connect(m_moveTransition, &QTimeLine::frameChanged, this, &CardUi::moveByPercent);
	connect(m_moveTransition, &QTimeLine::finished, this, &CardUi::finishMove);
	connect(m_moveTransition, &QTimeLine::finished, this, &CardUi::delayedTranslate);
	m_bufferSpeed = nullSpeed;
}

bool CardUi::isFaceUp() const { return m_faceUp; }

void CardUi::setCard(Card *card)
{
	if (m_moveTransition->state() == QTimeLine::Running){
		m_bufferCard = card;
	} else {
		m_card = card;
		update();
	}
}

QRectF CardUi::boundingRect() const
{
	return QRectF(0, 0, m_width, m_height);
}

void CardUi::paint(QPainter* painter, const QStyleOptionGraphicsItem*, QWidget *)
{
	painter->setRenderHint(QPainter::SmoothPixmapTransform, true);
	if (m_faceUp){
		painter->drawPixmap(0, 0, *m_manager->card(m_card));
	} else {
		painter->drawPixmap(0, 0, *m_manager->card());
	}
}

void CardUi::delayedTranslate()
{
	if (m_bufferSpeed != nullSpeed){
		double speed = m_bufferSpeed;
		m_bufferSpeed = nullSpeed;
		translate(m_bufferDest, m_bufferZValue, speed);
	}
}

void CardUi::translate(QPointF dest, double finalZValue) // no delay
{
	setPos(dest);
	setZValue(finalZValue);
	if (m_moveTransition->state() == QTimeLine::Running){
		m_moveTransition->stop();
	}
	m_bufferSpeed = nullSpeed;
	update();
}

int CardUi::translate(QPointF dest, double finalZValue, double speed)
{
	if (speed == nullSpeed){ return 0; }
	// if (speed == nullSpeed || speed <= 0){ translate(dest, finalZValue); }

	if (m_moveTransition->state() == QTimeLine::Running){
		m_bufferDest = dest;
		m_bufferZValue = finalZValue;
		m_bufferSpeed = speed;
		return m_moveTransition->duration() - m_moveTransition->currentTime();
	}

	m_moveEnd = dest;

	double distance = (m_moveEnd-pos()).manhattanLength();
	if (distance < 0){ distance = -distance; }
	if (distance < 1){
		translate(dest, finalZValue);
		return 0;
	}
	m_finalZValue = finalZValue;
	double duration = distance/speed;
	m_moveTransition->setDuration(duration);

	m_moveTransition->start();
	return duration;
}

void CardUi::finishMove()
{
	setZValue(m_finalZValue);
	if (m_bufferSpeed == nullSpeed){
		if (m_bufferFlip){
			m_bufferFlip = false;
			flip();
		}
		if (m_bufferCard != nullptr){
			Card* bufferCard = m_bufferCard;
			m_bufferCard = nullptr;
			setCard(bufferCard);
		}
	}

}

void CardUi::moveByPercent(int progress)
{
	double amount = (static_cast<double>(progress)/transitionMax);
	amount *= amount;
	auto movement = (m_moveEnd-pos())*amount;
	moveBy(movement.x(), movement.y());
	update();
}

void CardUi::setFaceDown()
{
	if (m_moveTransition->state() == QTimeLine::Running){
		if (m_faceUp){
			m_bufferFlip = true;
		} else {
			m_bufferFlip = false;
		}
	} else if (m_faceUp){
		flip();
	}
}

void CardUi::setFaceUp()
{
	if (m_moveTransition->state() == QTimeLine::Running){
		if (m_faceUp){
			m_bufferFlip = false;
		} else {
			m_bufferFlip = true;
		}
	} else if (!m_faceUp){
		flip();
	}
}

void CardUi::flip()
{
	if (m_moveTransition->state() == QTimeLine::Running){
		m_bufferFlip = !m_faceUp;
	} else {
		m_faceUp = !m_faceUp;
		update();
	}
}

void CardUi::changeParent(QGraphicsItem *parent)
{
	// change parent without changing its position on the screen.
	QPointF curPos = scenePos();
	setParentItem(parent);
	curPos -= scenePos();
	moveBy(curPos.x(), curPos.y());
	update();
}

void CardUi::updateImages()
{
	prepareGeometryChange();
	m_width = m_manager->width();
	m_height = m_manager->height();
	update();
}
