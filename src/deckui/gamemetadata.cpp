#include "deckui/gamemetadata.h"
#include "deckui/igameuidelegate.h"

GameMetadata::GameMetadata(IGameUiDelegate *game, QString type, QString name, QString rules)
	: m_game(game)
	, m_type(type)
	, m_name(name)
	, m_rules(rules)
{}

GameMetadata::GameMetadata(const GameMetadata &a)
{
	m_game = a.m_game;
	m_type = a.m_type;
	m_name = a.m_name;
	m_rules = a.m_rules;
}

IGameUiDelegate *GameMetadata::game() const { return m_game; }
QString GameMetadata::type() const { return m_type; }
QString GameMetadata::name() const { return m_name; }
QString GameMetadata::rules() const { return m_rules; }

bool GameMetadata::operator<(GameMetadata const& g)
{
	if (m_type == g.m_type){
		return m_name < g.m_name;
	} else {
		return m_type < g.m_type;
	}
}

bool GameMetadata::operator==(GameMetadata const& g){ return m_name == g.m_name; }
bool GameMetadata::operator>(GameMetadata const& g){ return !(operator<(g) && operator==(g)); }
bool GameMetadata::operator>=(GameMetadata const& g){ return !operator<(g); }
bool GameMetadata::operator<=(GameMetadata const& g){ return !operator>(g); }

void swap(GameMetadata &a, GameMetadata &b)
{
	IGameUiDelegate *game = a.m_game;
	QString type = a.m_type;
	QString name  = a.m_name;
	QString rules = a.m_rules;

	a.m_game = b.m_game;
	a.m_type = b.m_type;
	a.m_name = b.m_name;
	a.m_rules = b.m_rules;

	b.m_game = game;
	b.m_type = type;
	b.m_name = name;
	b.m_rules = rules;
}
