#include "deckui/gamemanager.h"
#include "deckui/gameloader.h"
#include "deckui/imagemanager.h"
#include "deckui/gameui.h"
#include "deckui/gamemetadata.h"
#include "deckui/igameuidelegate.h"
#include <QGraphicsView>
#include <QListView>
#include <QSize>
#include <QMenuBar>
#include <QAction>

GameManager::GameManager(int width, int height, QWidget *parent)
	: QMainWindow(parent)
	, m_gameUi(nullptr)
	, m_loader(new GameLoader())
	, m_gameSelection(new QListView())
	, m_imageManager(new ImageManager(width, height))
	, m_gameView(new QGraphicsView())
{
	setupUi();
}

void GameManager::setupUi()
{
	int width = m_imageManager->sceneWidth();
	int height = m_imageManager->sceneHeight();

	resize(width, height);

	m_gameSelection->setWrapping(true);
	m_gameSelection->setModel(m_loader);
	connect(m_gameSelection, &QAbstractItemView::activated, this, &GameManager::setGame);
	setCentralWidget(m_gameSelection);

	m_gameSelection->setFrameShape(QFrame::NoFrame);
	m_gameView->setFrameShape(QFrame::NoFrame);
	m_gameSelection->setMinimumSize(1,1); // would set to 0, 0 but that defaults to no minimum
	m_gameView->setMinimumSize(1,1);

	menuBar()->addAction(tr("Select Game Variant"));
	menuBar()->addAction(tr("Statistics"));
	menuBar()->addAction(tr("Run Algorithm"));
	menuBar()->addAction(tr("Settings"));
}

#include <QDebug>
void GameManager::setGame(const QModelIndex &index)
{
	if (!index.isValid()){ return; }
	if (m_gameUi != nullptr){ delete m_gameUi; }
	IGameUiDelegate * delegate = m_loader->metadata(index.row())->game();
	if (delegate == nullptr){ return; }
	delegate->setManager(m_imageManager);
	m_gameUi = new GameUi(m_imageManager, delegate);
	m_gameView->setScene(m_gameUi);
	m_gameView->resize(m_imageManager->sceneWidth(), m_imageManager->sceneHeight());
	setCentralWidget(m_gameView);

	connect(m_gameUi, &GameUi::gameOver, m_gameUi, &GameUi::newGame);
	connect(m_gameUi, &GameUi::gameOver, [=](bool isWon, int score){ 
			if (isWon){
				qDebug() << "Congrats, you won!";
				qDebug() << "Score:" << score;
			} else {
				qDebug() << "Well shucks, you only got" << score;
			}
		});
}

void GameManager::resizeEvent(QResizeEvent*){ m_imageManager->setSceneSize(centralWidget()->size()); }