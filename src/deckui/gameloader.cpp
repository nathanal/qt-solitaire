#include "deckui/gameloader.h"
#include "deckui/igameuidelegate.h"
#include "deckui/gamemetadata.h"
#include <QPluginLoader>
#include <QStaticPlugin>
#include <QJsonObject>
#include <QDir>
#include <QVector>
#include <algorithm>

GameLoader::GameLoader()
	: QAbstractListModel()
	, m_gameCount(0)
	, m_typeCount(0)
{
	// load static instances
	foreach (QStaticPlugin plugin, QPluginLoader::staticPlugins()){
		IGameUiDelegate* delegate = qobject_cast<IGameUiDelegate*>(plugin.instance());
		QJsonObject jsonData = plugin.metaData();
		add(delegate, jsonData);
	}
}

int GameLoader::rowCount(const QModelIndex &) const { return size(); }

Qt::ItemFlags GameLoader::flags(const QModelIndex &modelIndex) const
{
	if (!modelIndex.isValid()){ return Qt::NoItemFlags; }
	int i = modelIndex.row();
	if (m_data[i].game() == nullptr){
		return Qt::ItemNeverHasChildren | Qt::ItemIsEnabled;
	} else {
		return Qt::ItemNeverHasChildren | Qt::ItemIsSelectable | Qt::ItemIsEnabled;
	}
}

QVariant GameLoader::data(const QModelIndex &modelIndex, int role) const
{
	if (!modelIndex.isValid()){ return QVariant(); }
	int i = modelIndex.row();
	if (m_data[i].game() == nullptr){ // this is the type
		switch (role){
		case Qt::DisplayRole:
			return QVariant(m_data[i].type());			
		default:
			return QVariant();
		}
	} else { // this is the game
		switch (role){
		case Qt::DisplayRole:
			return QVariant(m_data[i].name());
		case Qt::ToolTipRole:
		case Qt::WhatsThisRole:
			return QVariant(m_data[i].rules());
		case Qt::UserRole:
			return modelIndex;
		default:
			return QVariant();
		}
	}
}

int GameLoader::size() const { return m_data.size(); }
int GameLoader::nTypes() const { return m_typeCount; }
int GameLoader::nGames() const { return m_gameCount; }
bool GameLoader::valid(int index) const { return (index >= 0 && index < size()); }
GameMetadata* GameLoader::metadata(int index) { return (valid(index) ? &m_data[index] : nullptr); }

bool GameLoader::loadFromDir(QString dirName)
{
	QDir dir(dirName);
	QStringList filters;
	filters << "lib*.so" << "lib*.dll" << "lib*.dylib";
	dir.setNameFilters(filters);

	QStringList fileNames = dir.entryList();
	bool success = false;
	foreach (QString fileName, fileNames){
		if (load(fileName)){
			success = true;
		}
	}
	return success;
}

bool GameLoader::load(QString fileName)
{
	QPluginLoader plugin(fileName);
	IGameUiDelegate* delegate = qobject_cast<IGameUiDelegate*>(plugin.instance());
	QJsonObject jsonData = plugin.metaData();
	if (add(delegate, jsonData)){
		return true;
	} else {
		plugin.unload();
		return false;
	}
}

bool GameLoader::add(IGameUiDelegate* delegate, const QJsonObject &jsonData)
{
	// inefficiences should be forgiven since there shouldn't be that many games
	if (!delegate){ return false; }
	QString type, name, rules;
	for (QJsonObject::const_iterator it = jsonData.constBegin(); it != jsonData.constEnd(); it++){
		if (it->type() == QJsonValue::Object){
			QJsonObject data = it->toObject();
			type = data.constFind("type")->toString(defaultType);
			name = data.constFind("name")->toString(invalid);
			rules = data.constFind("rules")->toString(defaultRules);
			break;
		}
	}
	if (name == ""){
		return false;
	}
	m_data.push_back(GameMetadata(delegate, type, name, rules));

	// Check that the game doesn't match an older game.
	// If it does, replace the old item.
	// Then exit
	for (int i=0; i<size()-1; i++){
		if (m_data[i] == m_data.back()){
			swap(m_data[i], m_data.back());
			m_data.pop_back();
			emit dataChanged(index(i), index(i));
			return true;
		}
	}

	// New game has been added
	m_gameCount ++;
	std::sort(m_data.begin(), m_data.end());
	int index = -1; // -1 is invalid, should never occur
	for (int i=0; i<size(); i++){
		if (m_data[i].name() == name){
			index = i;
			break;
		}
	}
	if (index == -1){ throw index; } // since the data has just been added, there should always be a valid index
	insertRow(index+1);

	// adds a row that just says the type if it doesn't already exist
	if ((index-1) < 0 || m_data[index-1].type() != type){
		m_typeCount ++;
		m_data.insert(m_data.begin()+index, GameMetadata(nullptr, type, invalid, invalid));
		insertRow(index);
	}

	return true;
}