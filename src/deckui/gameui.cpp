#include "deckui/gameui.h"

#include "deckui/cardui.h"
#include "deckui/imagemanager.h"
#include "deckui/igameuidelegate.h"

#include <QGraphicsSimpleTextItem>
#include <QGraphicsRectItem>
#include <QGraphicsSceneMouseEvent>
#include <QEvent>
#include <algorithm>
#include <vector>

GameUi::GameUi(ImageManager *manager, IGameUiDelegate *delegate, QObject *parent)
	: QGraphicsScene(0, 0, manager->sceneWidth(), manager->sceneHeight(), parent)
	, m_manager(manager)
	, m_delegate(delegate)
	, m_game(m_delegate->game())
	, m_paused(false)
	, m_newGameDelay(this)
{
	setBackgroundBrush(m_manager->background());
	m_delegate->buildConnections(this);

	connect(m_manager, &ImageManager::sceneResized, this, &GameUi::resize);

	m_newGameDelay.setSingleShot(true);
	connect(&m_newGameDelay, &QTimer::timeout, this, &GameUi::dealFromStock);

	// Build headers (e.g. Score text)
	auto items = m_delegate->buildHeaders();
	for (auto it=items.begin(); it != items.end(); it ++){
		addItem(*it);
	}
	m_delegate->updateHeaderPos(width(), height());
	m_delegate->calculatePilePos(width(), height());

	// Build cards
	int nCards = m_game->size();
	m_cards.reserve(nCards);
	for (int i=0; i<nCards; i++){
		m_cards.push_back( new CardUi(&Deck::invalidCard, m_manager) );
		addItem(m_cards.back());
		m_delegate->installFilter(m_cards.back());
		m_cards.back()->setZValue(i);
		m_cards.back()->setPile(IGame::Pile::STOCK);
		m_cards.back()->setIndex(i);
	}

	moveAllToStock();
	dealFromStock();
}

GameUi::~GameUi()
{
	for (int i=0; i<static_cast<int>(m_cards.size()); i++){
		delete m_cards[i];
	}
}

int GameUi::moveAllToStock()
{
	// assumes m_cards is ordered from lowest to highest z-value, at least for each pile
	// moves all cards to the stock pile, forcing them face down
	std::vector<CardUi*> reordered;
	int nCards = m_cards.size();
	reordered.reserve(nCards);
	// reorder cards already in stock
	for (int i=0; i<nCards; i++){
		m_cards[i]->setFaceDown();
		if (m_cards[i]->pile() == IGame::Pile::STOCK){
			m_cards[i]->setIndex(reordered.size());
			m_cards[i]->setZValue(reordered.size());
			m_cards[i]->translate(m_delegate->calculatePos(m_cards[i]), i);
			reordered.push_back(m_cards[i]);
			m_cards[i] = nullptr;
		}
	}

	// move other cards to stock
	int maxDuration = 0;
	for (int i=nCards-1; i>=0; i--){
		if (m_cards[i] == nullptr){ continue; }
		int duration = moveToPile(m_cards[i], IGame::Pile::STOCK, reordered.size(), m_defaultSpeed);
		maxDuration = (maxDuration > duration ? maxDuration : duration);
		m_cards[i]->setStatus(CardUi::Status::NO_MOVE, false);
		reordered.push_back(m_cards[i]);
	}

	m_cards.clear();
	std::swap(reordered, m_cards);
	return maxDuration;
}

void GameUi::updateCardAppearance()
{
	int nCards = m_game->size();
	for (int i=nCards-1; i>=0; i--){
		IGame::Pile pile = m_cards[i]->pile();
		int index = m_cards[i]->index();
		m_cards[i]->setCard( m_game->card(pile, index) );
		if (m_game->isCardVisible(pile, index)){
			m_cards[i]->setFaceUp();
			m_cards[i]->setStatus(CardUi::Status::NO_MOVE, false);
		}
	}
}

void GameUi::organiseScene()
{
	// assumes all cards are in order by z-value and in the stock.
	// Deal them into the correct pile
	int index = m_cards.size()-1;
	index = organisePile(IGame::Pile::LAYOUT, index);
	index = organisePile(IGame::Pile::RESERVE, index);
	index = organisePile(IGame::Pile::FOUNDATION, index);
	index = organisePile(IGame::Pile::WASTE, index);
	organisePile(IGame::Pile::CELLS, index);

	updateCardAppearance();
}

int GameUi::organisePile(IGame::Pile pile, int topDeckCardIndex)
{
	int nCards = m_game->pileInitSize(pile);
	for (int i=0; i<nCards; i++){
		moveToPile(m_cards[topDeckCardIndex], pile, i, m_defaultSpeed);
		topDeckCardIndex--;
	}
	return topDeckCardIndex;
}

void GameUi::prepForMove(CardUi *card, IGame::Pile pile, int pileIndex)
{
	card->setZValue(pileIndex + m_game->size()); // top card during movement
	card->setPile(pile);
	card->setIndex(pileIndex);
}

void GameUi::moveToPile(CardUi *card, IGame::Pile pile, int pileIndex)
{
	prepForMove(card, pile, pileIndex);
	card->translate(m_delegate->calculatePos(card), pileIndex);
}

int GameUi::moveToPile(CardUi *card, IGame::Pile pile, int pileIndex, double speed)
{
	prepForMove(card, pile, pileIndex);
	return card->translate(m_delegate->calculatePos(card), pileIndex, speed);
}

bool GameUi::move(CardUi* fromCard, IGame::Pile toPile, int toIndex)
{
	IGame::Pile fromPile = fromCard->pile();
	int fromIndex = fromCard->index();
	if (fromPile == toPile && fromIndex == toIndex){
		moveToPile(fromCard, fromPile, fromIndex, m_defaultSpeed);
		return true;
	} else if (m_game->move(fromPile, toPile, fromIndex, toIndex)){
		moveToPile(fromCard, toPile, toIndex, m_defaultSpeed);
		m_delegate->updateHeaderText();
		if (m_game->isGameOver()){
			if (m_game->isWin()){
				won();
			} else {
				lost();
			}
		}
		updateCardAppearance();
		return true;
	} else {
		return false;
	}
}

void GameUi::lost()
{
	pause();
	emit GameUi::gameOver(false, m_game->score());
}

void GameUi::won()
{
	pause();
	emit GameUi::gameOver(true, m_game->score());
}

void GameUi::newGame()
{
	if (m_newGameDelay.isActive()){ return; }
	unpause();
	m_newGameDelay.start(moveAllToStock()+m_delay);
}

void GameUi::dealFromStock()
{
	m_game->newGame();
	m_delegate->updateHeaderText();
	organiseScene();
}

void GameUi::pause()
{	
	m_paused = true;
	m_delegate->setPaused(true);
}

void GameUi::unpause()
{
	m_paused = false;
	m_delegate->setPaused(false);
}

bool GameUi::resize(QSize size)
{
	int width = size.width();
	int height = size.height();
	setBackgroundBrush(m_manager->background());
	setSceneRect(0, 0, width, height);
	m_delegate->updateHeaderPos(width, height);
	m_delegate->calculatePilePos(width, height);
	int nCards = m_cards.size();
	for (int i=0; i<nCards; i++){
		moveToPile(m_cards[i], m_cards[i]->pile(), m_cards[i]->index());
	}
	return true;
}