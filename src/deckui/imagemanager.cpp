#include "deckui/imagemanager.h"
#include <QColor>

ImageManager::ImageManager(double sceneWidth, double sceneHeight)
	: QObject()
	, m_width(0), m_height(0)
	, m_sceneWidth(sceneWidth)
	, m_sceneHeight(sceneHeight)
{
	auto fontId = QFontDatabase::addApplicationFont(defaultFontFile);
	m_font.setFamily(QFontDatabase::applicationFontFamilies(fontId).at(0));
	m_style = defaultStyle;
	m_dir = defaultDir;
	m_backgroundFile = defaultBackgroundFile;
	m_images.reserve(Card::cardsInDeck+1);
	setUpImages();
}

void ImageManager::setUpImages()
{
	std::vector<QPixmap*> images;
	if (!loadFromDir(images)){
		m_style = defaultStyle;
		clearVecOfPtrs(images);
		if (!loadFromDir(images)){
			clearVecOfPtrs(images);
			m_dir = defaultDir;
			loadFromDir(images);
		}
	}
	clearVecOfPtrs(m_images);
	std::swap(m_images, images);
	for (int i=0; i<static_cast<int>(images.size()); i++){
		delete images[i];
	}
}

bool ImageManager::loadFromDir(std::vector<QPixmap*> &images)
{
	if (m_width != 0 && m_height != 0 && (m_width < minLength || m_height < minLength)){
		makeDummyImages(images);
		return true;
	}
	QString name = fileName.arg(m_dir).arg(m_style);

	QPixmap *current;
	QPainter cardPainter;
	QSvgRenderer cardLoader;
	cardLoader.load(name.arg(downCardName));
	if (!cardLoader.isValid()){ return false; }
	// just update the height and width
	double height = cardLoader.defaultSize().height();
	double width = cardLoader.defaultSize().width();
	if (m_width == 0 || m_height == 0){
		m_width = width;
		m_height = height;
	} else {
		if (m_width < m_height * width/height){
			m_width = m_height * width/height;
		} else if (m_width > m_height * width/height){
			m_height = m_width * height/width;
		}
		if (height < m_height){
			height = m_height;
			width = m_width;
		}
	}
	// load and render image here
	for (int i=0; i<Card::cardsInDeck; i++){
		QString svgFileName = name.arg(Card(i).toString(true).toLower());
		cardLoader.load(svgFileName);
		if (!cardLoader.isValid()){ return false; }
		current = new QPixmap(m_width, m_height);
		cardPainter.begin(current);
		cardLoader.render(&cardPainter);
		cardPainter.end();

		images.push_back(current);
	}
	// load the face down image
	cardLoader.load(name.arg(downCardName));
	if (!cardLoader.isValid()){ return false; }
	current = new QPixmap(m_width, m_height);
	cardPainter.begin(current);
	cardLoader.render(&cardPainter);
	cardPainter.end();
	images.push_back(current);

	return true;
}

void ImageManager::makeDummyImages(std::vector<QPixmap*> &images)
{
	m_width = (m_width < 1) ? 1 : m_width;
	m_height = (m_height < 1) ? 1 : m_height;

	int hue, sat, val;
	QColor col;
	for (int i=0; i<Card::cardsInDeck; i++){
		QPixmap *canvas = new QPixmap(m_width, m_height);
		QPainter cardPainter(canvas);

		Card card(i);
		switch (card.suit()){
		case Card::Suit::DIAMONDS:
			hue = 0; sat = 85;
			break;
		case Card::Suit::CLUBS: 
			hue = 240; sat = 85;
			break;
		case Card::Suit::HEARTS:
			hue = 0; sat = 170;
			break;
		case Card::Suit::SPADES:
		default:
			hue = 240; sat = 170;
			break;
		}
		val = (card.value()+1)*255.0/(Card::cardsInSuit+2);
		col.setHsv(hue, sat, val);
		cardPainter.fillRect(0, 0, m_width, m_height, col);
		images.push_back(canvas);
	}
	QPixmap *canvas = new QPixmap(m_width, m_height);
	QPainter cardPainter(canvas);
	hue = 120; sat = 42; val = 125;
	col.setHsv(hue, sat, val);
	cardPainter.fillRect(0, 0, m_width, m_height, col);
	images.push_back(canvas);
}

void ImageManager::clearVecOfPtrs(std::vector<QPixmap *> &vec) // can be generalised with template, no problem
{
	for (auto it = vec.begin(); it < vec.end(); it++){
		delete *it;
	}
	vec.clear();
}

void ImageManager::setWidth(double width)
{
	if (width <= 0 || width == m_width){ return; }

	m_height = (m_width==0) ? m_width : width*m_height/m_width;
	m_width = width;
	setUpImages();
	emit changedImage();
}

void ImageManager::setHeight(double height)
{
	if (height <= 0 || height == m_height){ return; }
	m_width = (m_height==0) ? height : height*m_width/m_height;
	m_height = height;
	setUpImages();
	emit changedImage();
}

void ImageManager::setBackground(QString backgroundFile)
{
	if (!QPixmap(backgroundFile).isNull()){
		m_backgroundFile = backgroundFile;
	}
}

void ImageManager::setSceneSize(double width, double height){ setSceneSize(QSize(width, height)); }
void ImageManager::setSceneSize(QSize size)
{
	if (size.width() < minSceneWidth){ size.setWidth(minSceneWidth); }
	if (size.height() < minSceneHeight){ size.setHeight(minSceneHeight); }
	m_sceneWidth = size.width();
	m_sceneHeight = size.height();
	emit sceneResized(size);
	emit changedImage();
}

void ImageManager::setDir(QString dir)
{
	if (m_dir == dir){ return; }
	m_dir = dir;
	setUpImages();
	emit changedImage();
}

void ImageManager::setStyle(QString style)
{
	if (m_style == style){ return; }
	m_style = style;
	setUpImages();
	emit changedImage();
}

double ImageManager::width() const { return m_width; }
double ImageManager::height() const { return m_height; }
double ImageManager::sceneWidth() const { return m_sceneWidth; }
double ImageManager::sceneHeight() const { return m_sceneHeight; }
QString ImageManager::dir() const { return m_dir; }
QString ImageManager::style() const { return m_style; }

QPixmap* ImageManager::card(const Card *card) const
{
	int index = Card::cardsInDeck; // 52 for Face down
	if (card != nullptr && card->id() >= 0){ index = card->id(); }
	return m_images[index];
}

QGraphicsSimpleTextItem* ImageManager::text(QString text, qreal height)
{
	m_font.setPixelSize(height);
	QGraphicsSimpleTextItem *item = new QGraphicsSimpleTextItem(text);
	item->setBrush(Qt::black);
	item->setFont(m_font);
	return item;
}

QBrush ImageManager::background()
{
	QPixmap background(m_backgroundFile);
	// ensure the loaded file has >= dimensions to the scene
	double widthRatio = m_sceneWidth/background.width();
	double heightRatio = m_sceneHeight/background.height();
	if (widthRatio > 1 && (heightRatio < 1 || widthRatio > heightRatio)){
		background = QPixmap(background.scaledToWidth(m_sceneWidth, Qt::SmoothTransformation));
	} else if (heightRatio > 1 && (widthRatio < 1 || heightRatio > widthRatio)){
		background = QPixmap(background.scaledToHeight(m_sceneHeight, Qt::SmoothTransformation));
	}

	// crop out the pixmap that is visible.
	int x = ((background.width() - m_sceneWidth)/2);
	int y = ((background.height() - m_sceneHeight)/2);
	background = QPixmap( background.copy(x, y, m_sceneWidth, m_sceneHeight) );
	return QBrush(background);
}