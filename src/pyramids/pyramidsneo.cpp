#include "pyramids/pyramidsneo.h"

PyramidsNeo::PyramidsNeo() 
	: AbstractPyramids()
{
	init();
	newGame();
}

PyramidsNeo::PyramidsNeo(int seed)
	: AbstractPyramids()
{
	init();
	newGame(seed);
}

void PyramidsNeo::init()
{
	m_scoring[0] = 502;
	m_scoring[1] = 27;
	m_scoring[2] = 17;
	m_scoring[3] = 12;
	m_scoring[4] = 7;
	m_scoring[5] = 4;
	m_scoring[6] = 3;
	m_scoring[7] = 0; // null
	m_lastMoved = layersInLayout;
	AbstractPyramids::init();
}

void PyramidsNeo::updateScore(Card *card)
{
	if (card == nullptr){
		m_streak = 0;
	} else {
		m_streak++;
		m_score += m_scoring[m_lastMoved] + m_streak*m_streak;
	}
}

bool PyramidsNeo::move(Pile from, Pile to, int fromInd, int toInd)
{
	if (to != Pile::WASTE || toInd != pileSize(Pile::WASTE)){
		return false; // in this game of solitaire, cards can only go to the top of the waste.
	}
	switch (from){
	case Pile::LAYOUT:
		if (isCardMovable(from, fromInd)){
			m_waste.push_back(card(from, fromInd));
			m_layout[toY(fromInd)][toX(fromInd)] = nullptr;
			m_lastMoved = toY(fromInd);
			updateScore(m_waste.back());
			return true;
		}
		break;
	case Pile::STOCK:
		if (fromInd + 1 == static_cast<int>(m_stock.size()) && fromInd >= 0){
			m_waste.push_back(m_stock.back());
			m_stock.pop_back();
			m_lastMoved = layersInLayout;
			updateScore();
			return true;
		}
	default:
		break;
	}
	return false;
}

bool PyramidsNeo::isGameOver() const
{
	if (isWin()){ return true; }
	if (isCardThere(Pile::STOCK, 0)){ return false; }
	for (int i=0; i<cardsInLayout; i++){
		if (isCardMovable(Pile::LAYOUT, i)){
			return false;
		}
	}
	return true;
}

bool PyramidsNeo::isCardVisible(Pile pile, int index) const
{
	return isCardThere(pile, index) && card(pile, index)->isValid();
}

bool PyramidsNeo::isCardMovable(Pile pile, int index) const
{
	switch (pile){
	case Pile::LAYOUT:
		if (!isCardVisible(pile, index)) { return false; }
		{
		Card *w = topOfWaste(); // never nullptr since game starts with something here
		Card *l = card(pile, index);
		return (l->adjNumber(*w, true));
		}
	case Pile::STOCK:
		return (m_stock.size() != 0); // ignores index, will always just pick next card.
	default:
		return false;	
	}
}

Card* PyramidsNeo::card(Pile pile, int index) const
{
	if (!validIndex(pile, index)){ return nullptr; }
	switch (pile){
	case Pile::WASTE:
		if (index < pileSize(pile)){
			return m_waste[index];
		} else {
			return &Deck::invalidCard;
		}
	case Pile::STOCK:
		return &Deck::invalidCard;
	case Pile::LAYOUT:
		{
		int y = toY(index);
		int x = toX(index);
		Card *c = m_layout[y][x];		
		if (c == nullptr || y+1 == layersInLayout){	
			return c;
		} else {
			if (m_layout[y+1][x] == nullptr && m_layout[y+1][x+1] == nullptr){
				return c;
			} else {
				return &Deck::invalidCard;
			}
		}
		}
	default:
		return nullptr;
	}
}