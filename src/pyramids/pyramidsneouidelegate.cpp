#include "pyramids/pyramidsneouidelegate.h"
#include "pyramids/pyramidsneo.h"

PyramidsNeoUiDelegate::PyramidsNeoUiDelegate()
	: AbstractPyramidsUiDelegate()
{
	m_game = new PyramidsNeo();
}

void PyramidsNeoUiDelegate::calculatePilePos(double width, double height)
{
	// calculate the top left point of the pile position
	if (m_manager == nullptr){ return; }
	double cardWidth = m_manager->width();
	double cardHeight = m_manager->height();
	double heightInCards = 37.0/8;
	double widthInCards = 17.0/2;
	double marginToHeaderRatio = 0.6;
	double marginHeight = m_headerHeight*marginToHeaderRatio;
	
	// adjust card size to fit the layout
	double idealCardWidth = (width - 2*marginHeight)/widthInCards;
	double idealCardHeight = (height - m_headerHeight - 2*marginHeight)/heightInCards;
	double idealWidthRatio = idealCardWidth/cardWidth;
	double idealHeightRatio = idealCardHeight/cardHeight;
	if (idealWidthRatio < idealHeightRatio){
		m_manager->setWidth(idealCardWidth);
		cardWidth = idealCardWidth;
		cardHeight = m_manager->height();
	} else {
		m_manager->setHeight(idealCardHeight);
		cardHeight = idealCardHeight;
		cardWidth = m_manager->width();
	}

	m_stockPos = QRectF(width/2, marginHeight/*+(cardHeight*29/8)*/, cardWidth, cardHeight);
	m_wastePos = m_stockPos;
	m_stockPos.translate(-cardWidth*9/8, 0);
	m_wastePos.translate(cardWidth/8, 0);
	m_layoutPos = QRectF(width/2, marginHeight+(cardHeight*11/8), cardWidth*17/2, cardHeight*13/4);
	m_layoutPos.translate(-cardWidth*17/4, 0);
}

QPointF PyramidsNeoUiDelegate::calculatePos(const CardUi *c)
{
	if (m_manager == nullptr){ return QPointF(); }
	double cardWidth = m_manager->width();
	double cardHeight = m_manager->height();

	IGame::Pile pile = c->pile();
	int index = c->index();
	switch (pile){
	case IGame::Pile::LAYOUT:
		{
		QPointF p = m_layoutPos.topLeft();
		int yCards = m_game->toY(index);
		int xCards = m_game->toX(index);

		p.setY(p.y() + (yCards*cardHeight*3/8) );
		double startX = (8.5-(yCards+1.0)-(yCards/4.0))/2.0;
		startX += 1.25*xCards;
		startX *= cardWidth;
		p.setX(p.x() + startX);
		return p;
		}
	case IGame::Pile::STOCK:
		return m_stockPos.topLeft() + (m_spread*index);
	case IGame::Pile::WASTE:
	default:
		return m_wastePos.topLeft() + (m_spread*index);
	}
}


bool PyramidsNeoUiDelegate::eventFilter(QObject *watched, QEvent *event)
{
	if (m_paused){ return false; }
	if (event->type() == QEvent::GraphicsSceneMouseMove || 
		event->type() == QEvent::GraphicsSceneMousePress ||
		event->type() == QEvent::GraphicsSceneMouseRelease){
		if (static_cast<QGraphicsSceneMouseEvent*>(event)->button() != Qt::LeftButton){ return false; }
	}

	CardUi *curCard = static_cast<CardUi*>(watched);
	if (curCard){
		IGame::Pile pile = curCard->pile();
		int index = curCard->index();
		switch (pile){
		case IGame::Pile::LAYOUT:
		case IGame::Pile::STOCK:
			if (event->type() == QEvent::GraphicsSceneHoverEnter){
				if (pile == IGame::Pile::LAYOUT && m_game->isCardMovable(pile, index)){
					curCard->setStatus(CardUi::Status::VALID_MOVE, true);
				} else if (pile == IGame::Pile::STOCK && index+1 == m_game->pileSize(pile)){
					curCard->setStatus(CardUi::Status::NO_MOVE, true);
				}
			} else if (event->type() == QEvent::GraphicsSceneHoverLeave){
				curCard->setStatus(CardUi::Status::NO_MOVE, false);
			} else if (event->type() == QEvent::GraphicsSceneMousePress){
				emit move(curCard, IGame::Pile::WASTE, m_game->pileSize(IGame::Pile::WASTE));
				return true;
			}
			break;
		case IGame::Pile::WASTE:
			if (event->type() == QEvent::GraphicsSceneMousePress){
				curCard->setFlag(QGraphicsItem::ItemIsMovable, true);
				return true;
			} else if (event->type() == QEvent::GraphicsSceneMouseRelease){
				if (curCard->flags()){
					curCard->setFlag(QGraphicsItem::ItemIsMovable, false);
					emit move(curCard, curCard->pile(), curCard->index());
					return true;
				}
			}
			break;
		default:
			break;
		}
	}
	return false;
}