#include "pyramids/abstractpyramidsuidelegate.h"

AbstractPyramidsUiDelegate::AbstractPyramidsUiDelegate()
	: QObject()
	, m_manager(nullptr)
	, m_paused(false)
	, m_spread(-0.2, -0.2)
{}

AbstractPyramidsUiDelegate::~AbstractPyramidsUiDelegate()
{
	delete m_game;
}

void AbstractPyramidsUiDelegate::setManager(ImageManager *manager){ m_manager = manager; }

std::vector<QGraphicsItem*> AbstractPyramidsUiDelegate::buildHeaders()
{
	if (m_manager == nullptr){ return std::vector<QGraphicsItem*>(); }
	double textHeight = m_headerHeight/2;

	// build header
	headerItem = new QGraphicsRectItem();
	headerItem->setPen(Qt::NoPen);
	headerItem->setBrush(QColor(255, 255, 255, 200));

	scoreText = tr("Score: %1");
	cumulativeText = tr("Cumulative: %1");
	stockText = tr("Cards left: %1");

	scoreItem = m_manager->text(scoreText.arg(m_game->score()), textHeight);
	cumulativeItem = m_manager->text(cumulativeText.arg(m_game->streak()), textHeight);
	stockItem = m_manager->text(stockText.arg(m_game->pileSize(IGame::Pile::STOCK)), textHeight);

	std::vector<QGraphicsItem*> toAdd;
	toAdd.push_back(headerItem);
	toAdd.push_back(scoreItem);
	toAdd.push_back(cumulativeItem);
	toAdd.push_back(stockItem);
	return toAdd;
}

void AbstractPyramidsUiDelegate::updateHeaderPos(const double width, const double height)
{
	double textHeight = m_headerHeight/2;
	double linePos = (m_headerHeight-textHeight)*0.625;
	headerItem->setRect(0, height - m_headerHeight, width, m_headerHeight);

	scoreItem->setPos(m_margin, height - linePos - textHeight);
	cumulativeItem->setPos((width - cumulativeItem->boundingRect().width())/2, height - linePos - textHeight);
	if (scoreItem->sceneBoundingRect().intersects(cumulativeItem->sceneBoundingRect())){
		cumulativeItem->moveBy(0, m_headerHeight);
	}
	stockItem->setPos(width - stockItem->boundingRect().width() - m_margin, height - linePos - textHeight);
	if (scoreItem->sceneBoundingRect().intersects(stockItem->sceneBoundingRect())){
		stockItem->moveBy(0, m_headerHeight*2);
	} else if (cumulativeItem->sceneBoundingRect().intersects(stockItem->sceneBoundingRect())){
		cumulativeItem->moveBy(0, m_headerHeight);
	}
}

void AbstractPyramidsUiDelegate::updateHeaderText()
{
	scoreItem->setText(scoreText.arg(m_game->score()));
	cumulativeItem->setText(cumulativeText.arg(m_game->streak()));
	stockItem->setText(stockText.arg(m_game->pileSize(IGame::Pile::STOCK)));
}

QRectF AbstractPyramidsUiDelegate::pilePos(IGame::Pile pile)
{
	switch (pile){
		case IGame::Pile::LAYOUT:
			return m_layoutPos;
		case IGame::Pile::STOCK:
			return m_stockPos;
		case IGame::Pile::WASTE:
			return m_wastePos;
		case IGame::Pile::CELLS:
			return m_cellsPos;
		case IGame::Pile::RESERVE:
			return m_reservePos;
		case IGame::Pile::FOUNDATION:
		default:
			return m_foundationPos;
	}
	return QRectF(); // never called
}

IGame* AbstractPyramidsUiDelegate::game() { return m_game; }
void AbstractPyramidsUiDelegate::setPaused(bool paused) { m_paused = paused; }

void AbstractPyramidsUiDelegate::installFilter(QGraphicsObject *watched) { watched->installEventFilter(this); }

void AbstractPyramidsUiDelegate::buildConnections(GameUi *obj)
{
	connect(this, &AbstractPyramidsUiDelegate::move, obj, &GameUi::move);
}
