#include "pyramids/abstractpyramids.h"

void AbstractPyramids::init()
{
	m_layout.resize(layersInLayout);
	for (int i=0; i<layersInLayout; i++){
		m_layout[i].resize(i+1);
	}
	m_stock.reserve(cardsInStock);
	m_waste.reserve(Card::cardsInDeck);
}

void AbstractPyramids::reset()
{
	m_score = 0;
	m_streak = 0;
	m_stock.clear();
	m_waste.clear();
	int index = 0;
	for (int i=0; i<layersInLayout; i++){
		for (int j=0; j<=i; j++){
			m_layout[i][j] = &m_deck[index++];
		}
	}
	for (int i=0; i<cardsInStock; i++){
		m_stock.push_back(&m_deck[index++]);
	}
	m_waste.push_back(&m_deck[index]);
}

void AbstractPyramids::newGame(bool continueGame)
{
	m_deck.shuffle(continueGame);
	reset();
}

void AbstractPyramids::newGame(int seed)
{
	m_deck.shuffle(seed);
	reset();
}

void AbstractPyramids::restartGame()
{
	reset();
}

bool AbstractPyramids::load(QString saveFile)
{
	int splitPos = saveFile.indexOf(Deck::invalidBase64Char);
	if (splitPos == -1){
		return false;
	}

	// generateCards
	Deck deck = Deck();
	if (!deck.load(saveFile.right(saveFile.size() - splitPos - 1))){
		return false;
	}
	QString currentGameSave = save();
	m_deck = deck;

	// generate layout
	QByteArray data;
	data.append(saveFile.left(splitPos));
	data = qUncompress(QByteArray::fromBase64(data));
	int counter = cardsInLayout;
	bool processingLayout = true;
	bool processingStock = false;
	bool processingWaste = false;
	int index = 0;
	int i = 0; // layout coord
	int j = 0; // layout coord
	int totalCards = 0;
	std::vector<bool> cardUsed;
	cardUsed.resize(Card::cardsInDeck);
	std::fill(cardUsed.begin(), cardUsed.end(), false);
	while (processingLayout || processingStock || processingWaste){
		counter --;
		int pos = data[index++];

		if (pos < 0 || pos > Card::cardsInDeck || (pos == 0 && !processingLayout)){
			break;
		} else if (pos != 0){
			cardUsed[pos-1] = true;
			totalCards++;
		}
		if (processingLayout){
			if (pos == 0){
				m_layout[i][j] = nullptr;
			} else {
				m_layout[i][j] = &m_deck[pos-1];
			}
			j ++;
			if (j > i){
				i ++;
				j = 0;
			}
			if (counter == 0){
				counter = data[index++];
				if (counter > cardsInStock){ // max num of cards in the stock pile
					break;
				}
				bool validLayout = true;
				for (int index=0; index<cardsInLayout; index++){
					if (!isCardThere(Pile::LAYOUT, index)){
						int y = toY(index);
						int x = toX(index);
						if (y+1 == layersInLayout){
							continue;
						} else if ((m_layout[y+1][x] != nullptr) || (m_layout[y+1][x+1] != nullptr)){
							validLayout = false;
							break;
						}
					}
					if (!validLayout){ break; }
				}
				if (!validLayout){ break; }
				processingLayout = false;
				processingStock = true;
				if (counter <= 0){
					counter = data[index++];
					processingStock = false;
					processingWaste = true;
				}
			}
		} else if (processingStock){
			m_stock.push_back(&m_deck[pos-1]);
			if (counter <= 0){
				counter = data[index++];
				processingStock = false;
				processingWaste = true;
			}
		} else if (processingWaste){
			m_waste.push_back(&m_deck[pos-1]);
			if (counter <= 0){
				m_streak = 0;
				for (int i=0; i<static_cast<int>(sizeof(m_streak)); i++){
					m_streak += ((static_cast<int>(data[index])) << (i*8));
					index++;
				}
				m_score = 0;
				for (int i=0; i<static_cast<int>(sizeof(m_score)); i++){
					m_score += ((static_cast<int>(data[index+i])) << (i*8));
				}
				processingWaste = false;
			}
		}
	}
	if (processingStock || processingWaste || processingLayout ||
		(totalCards != Card::cardsInDeck) ||
		(std::count(cardUsed.begin(), cardUsed.end(), true) != Card::cardsInDeck)){
		load(currentGameSave);
		return false;
	} else {
		return true;
	}
}

int AbstractPyramids::findCard(Card* card) const
{
	if (card == nullptr){
		return 0;
	} else {
		// returns 1-based array index
		for (int i=0; i<m_deck.size(); i++){
			if (&(m_deck[i]) == card){
				return i+1;
			}
		}
		return 0;
	}
}

QString AbstractPyramids::save() const
{
	QByteArray data;
	for (int i=0; i<layersInLayout; i++){
		for (int j=0; j<=i; j++){
			if (m_layout[i][j] == nullptr){
				data.append(static_cast<char>(int(0)));
			} else {
				data.append(static_cast<char>(findCard(m_layout[i][j])));
			}
		}
	}
	data.append(static_cast<char>(pileSize(Pile::STOCK)));
	for (int i=0; i<pileSize(Pile::STOCK); i++){
		data.append(static_cast<char>(findCard(m_stock[i])));
	}
	data.append(static_cast<char>(pileSize(Pile::WASTE)));
	for (int i=0; i<pileSize(Pile::WASTE); i++){
		data.append(static_cast<char>(findCard(m_waste[i])));
	}
	for (int i=0; i<static_cast<int>(sizeof(m_streak)); i++){
		data.append((((m_streak & (0xFF << (i*8))) >> (i*8))));
	}
	for (int i=0; i<static_cast<int>(sizeof(m_score)); i++){
		data.append((((m_score & (0xFF << (i*8))) >> (i*8))));
	}
	return QString(qCompress(data).toBase64()) + QString(Deck::invalidBase64Char) + m_deck.save();
}

bool AbstractPyramids::validTriangularCoordinate(int i, int j) const { return (i < layersInLayout && i >=0 && j <= i && j >= 0); }
bool AbstractPyramids::validIndex(Pile p, int i) const { return i >= 0 && i < pileSize(p); }

int AbstractPyramids::toFlatArr(int x, int y) const
{
	if (!validTriangularCoordinate(x, y)){ return invalid; }
	return (((x+1)*x)/2) + y; 
}

int AbstractPyramids::toY(int i) const { return toTriangularArray(i, false); }
int AbstractPyramids::toX(int i) const { return toTriangularArray(i, true); }
int AbstractPyramids::toTriangularArray(int i, bool hor) const
{
	if (!validIndex(Pile::LAYOUT, i)){ return invalid; }
	int y = 0;
	while (y < i){
		i-= (++y);
	}
	int x = i;
	return (hor ? x : y);
}

bool AbstractPyramids::pileExists(Pile pile) const
{
	switch (pile){
	case Pile::STOCK:
	case Pile::WASTE:
	case Pile::LAYOUT:
		return true;
	default:
		return false;
	}
}

int AbstractPyramids::size() const { return m_deck.size(); }

int AbstractPyramids::pileInitSize(Pile pile) const
{
	switch (pile){
	case Pile::STOCK:
		return cardsInStock;
	case Pile::WASTE:
		return 1;
	case Pile::LAYOUT:
		return cardsInLayout;
	default:
		return 0;
	}
}

int AbstractPyramids::pileSize(Pile pile) const
{
	switch (pile){
	case Pile::STOCK:
		return m_stock.size();
	case Pile::WASTE:
		return m_waste.size();
	case Pile::LAYOUT:
		return cardsInLayout;
	default:
		return 0;
	}
}

bool AbstractPyramids::isCardThere(Pile pile, int index) const
{
	return (card(pile, index) != nullptr);
}

Card* AbstractPyramids::topOfWaste() const
{
	if (m_waste.size() == 0){
		return nullptr;
	} else {
		return m_waste.back();
	}
}

int AbstractPyramids::score() const { return m_score; }
int AbstractPyramids::streak() const { return m_streak; }

bool AbstractPyramids::isWin() const { return !isCardThere(Pile::LAYOUT, 0); }