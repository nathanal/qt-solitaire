# QT Solitaire (not ready for testing)

Currently can play a game of pyramids (neo edition), no score/end screen/restart game/options/customisability

## Goal

Suite of solitaire games, ideally playable via CLI or GUI.

Accepts plugin that will play the game along an algorithm.

Will attempt to add add least klondite, free cell, and pyramids, though if I have time (and learn the rules of these games): demon, poker squares, cribbage squares, golf, casket, alternations, spider, eliminator, yukon, tripeaks, and non-solitaire games (blackjack) may be added.

### Short term to do
add GameManager class
    Add new game, load, save options
    add stats
    add end screen dialog
Implement default screen as Model/View
    Current bugs:
        bad default screen (modify QFont)
Correct J, Q, K card images, make the edges text bigger!

Implement game player algorithm option

Need to implement proper error catching in loading old games from save data.
Consider a default UI (can't play from it) for games with no UI (i.e. can still use algorithm)

Allow non QT plugin of the algorithm and game to be loaded.
Use std::shared_ptr.

### Non-critical bugs
On resize, QPainter errors, see tiny-widget-test (local file)

## Structure
Games are stored as non-QT classes that use the IGame interface (see include/deck/igame.h). They must be used with Qt-based plugins describing the UI logic (see include/deckui/gameui.h and include/deckui/igameuidelegate.h).

Algorithms for playing games must use the IGameAlgorithm interface (see include/deck/igamealgorithm.h)

The GameManager class is maintains the main window and deals with settings and loading algorithms. This is done by including the GameLoader class (loads plugins), ???

### Game logic
[complete,tested] Card class: holds a card or null card.
Converts card to string (e.g. `ace of spades`) or unicode character (e.g. `🂡`)

[complete,tested] Deck class: holds a flat vector containing `n` decks.
Shuffles cards, provides simple save and restore of deck order.
Saving obfuscates card order but makes no attempt at encryption/preventing players from reverse engineering the hidden cards.

[complete,untested] IGame class: interface to build new solitaire games

[complete,untested] AbstractPyramids class: implementation of IGame with rules for pyramids.

[complete,partial testing] PyramidsNeo class : implementation of a game of pyramids, following the rules of the neopets version (not sure if scoring is accurate).

### UI
Note that this is not an analogue to the game logic. CardUi holds details known from card, deck and game logic, but the GameUi manages the CardUi objects.

ImageDialogue and GameManager should have single instances.

[incomplete,revamp] ImageManager class: Cache and manager resources.

[influx] CardUi class: manages card display, not tied to a game implementation
[suggestion]Consider changing the flip transition, ~100 blocks that are randomly moved from faceUp to faceDown over the few seconds.
[note] the Card* pointer is kinda unnecessary. If we don't ever need to flip cards over, then it is definitely unnecessary!

[incomplete,influx] GameUi class: manages a game (take user input, manage GUI)
Consider turning into an interface. Has not been updated to use ImageManager yet.

### Credit
Fonts used in custom Playing Cards: Fira Sans for all but the small capital J (which uses Deja Vu Sans)
Alien.png was created by Nathan Lee, released in the public domain.

### GUI considerations
Make the white bg over text just the width of the text (opposed to the whole horizontal bar).
Make the options (e.g. New, Reset, Quit) part of the Main Window options (try as duplication of options and as the only other option.
For PyramidsNeo, consider moving stock and waste to the bottom [considered, not recommended based on appearance]
Consider the bottom layer as: Score: Consecutive Cards: Cards Left: 
        or Cards Left: Consecu...: Score:
