#ifndef ABSTRACT_PYRAMIDS_UI_DELEGATE_H
#define ABSTRACT_PYRAMIDS_UI_DELEGATE_H

#include "deckui/igameuidelegate.h"
#include <QGraphicsItem>
#include <QGraphicsSimpleTextItem>
#include <QGraphicsRectItem>
#include <QGraphicsObject>
#include <QEvent>
#include <QGraphicsSceneMouseEvent>
#include "deck/igame.h"
#include "deckui/cardui.h"
#include "deckui/gameui.h"
#include "deckui/imagemanager.h"
#include "pyramids/abstractpyramids.h"

class AbstractPyramidsUiDelegate : public QObject, public IGameUiDelegate
{
	Q_OBJECT
protected:
	AbstractPyramids *m_game;
	ImageManager *m_manager;
	bool m_paused;
	QRectF m_stockPos, m_layoutPos, m_wastePos;
	QRectF m_cellsPos, m_reservePos, m_foundationPos;
	QGraphicsSimpleTextItem *scoreItem, *cumulativeItem, *stockItem;
	QGraphicsRectItem *headerItem;
	QString scoreText, cumulativeText, stockText;
	const int m_margin = 10;
	const double m_headerHeight = 45;
	const QPointF m_spread;
public:
	AbstractPyramidsUiDelegate();
	virtual ~AbstractPyramidsUiDelegate();
	void setManager(ImageManager*);

	std::vector<QGraphicsItem*> buildHeaders();
	void updateHeaderPos(const double width, const double height);
	void updateHeaderText();

	virtual void calculatePilePos(double width, double height) = 0;
	virtual QPointF calculatePos(const CardUi *) = 0;
	QRectF pilePos(IGame::Pile);

	IGame* game();
	void setPaused(bool);
	virtual bool eventFilter(QObject *watched, QEvent *event) = 0;
	void installFilter(QGraphicsObject *watched);
	void buildConnections(GameUi*);
signals:
	bool move(CardUi *from, IGame::Pile toPile, int toIndex);
};

#endif // ABSTRACT_PYRAMIDS_UI_DELEGATE_H