#ifndef PYRAMIDS_NEO_UI_DELEGATE_H
#define PYRAMIDS_NEO_UI_DELEGATE_H

#include "pyramids/abstractpyramidsuidelegate.h"

class PyramidsNeoUiDelegate : public AbstractPyramidsUiDelegate
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID IGameUiDelegate_iid FILE "pyramids/pyramidsneoui.json")
	Q_INTERFACES(IGameUiDelegate)
public:
	PyramidsNeoUiDelegate();
	~PyramidsNeoUiDelegate() {}

	void calculatePilePos(double width, double height);
	QPointF calculatePos(const CardUi *);

	bool eventFilter(QObject *watched, QEvent *event);
};

#endif // PYRAMIDS_NEO_UI_DELEGATE_H