#ifndef ABSTRACT_PYRAMIDS_H
#define ABSTRACT_PYRAMIDS_H

#include "deck/card.h"
#include "deck/deck.h"
#include "deck/igame.h"
#include <vector>

class AbstractPyramids : public IGame
{
protected:
	int m_score;
	int m_streak;
	Deck m_deck;

	static const int cardsInLayout = 28;
	static const int layersInLayout = 7;
	static const int cardsInStock = Card::cardsInDeck - cardsInLayout - 1;
	static const int invalid = -1;

	std::vector<std::vector<Card*>> m_layout;
	std::vector<Card*> m_stock;
	std::vector<Card*> m_waste;

	virtual void init();
	virtual void reset();
	virtual void updateScore(Card* card=nullptr) = 0;
	int findCard(Card* card) const; // used in load/save operation
	
public:
	AbstractPyramids() : IGame() {};
	virtual ~AbstractPyramids() {}; // pointers point to cards in m_deck
	// Write actions
	void newGame(bool continueGame=false);
	void newGame(int seed);
	void restartGame();

	virtual bool load(QString);
	virtual QString save() const;
	bool move(Pile from, Pile to, int fromInd = invalid, int toInd = invalid) = 0;

	// Read actions
	virtual bool pileExists(Pile) const;
	virtual int size() const;
	virtual int pileInitSize(Pile) const;
	virtual int pileSize(Pile) const;

	bool validTriangularCoordinate(int, int) const;
	bool validIndex(Pile, int) const;
	int toFlatArr(int, int) const;
	int toTriangularArray(int i, bool hor) const;
	int toY(int) const;
	int toX(int) const;

	virtual bool isCardThere(Pile, int) const;
	virtual bool isCardVisible(Pile, int) const = 0;
	virtual bool isCardMovable(Pile, int i=invalid) const = 0;

	virtual Card* topOfWaste() const;
	virtual Card* card(Pile, int) const = 0;
		// return nullptr if not a card
		// return Deck::invalidCard if we shouldn't be able to know it

	int streak() const;
	virtual int score() const;
	virtual bool isWin() const;
	virtual bool isGameOver() const = 0;
};

#endif // ABSTRACT_PYRAMIDS_H