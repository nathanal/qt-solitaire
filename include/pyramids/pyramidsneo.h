#ifndef PYRAMIDS_NEO_H
#define PYRAMIDS_NEO_H
#include "pyramids/abstractpyramids.h"

class PyramidsNeo : public AbstractPyramids
{
	int m_scoring[layersInLayout+1];
	int m_lastMoved;
	void updateScore(Card* card=nullptr);
public:
	PyramidsNeo();
	PyramidsNeo(int seed);

	void init();
	bool move(Pile from, Pile to, int fromInd = invalid, int toInd = invalid);

	virtual bool isCardVisible(Pile, int) const;
	virtual bool isCardMovable(Pile, int i=invalid) const;

	virtual Card* card(Pile, int) const;

	virtual bool isGameOver() const;

};

#endif // PYRAMIDS_NEO_H