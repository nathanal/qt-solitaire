#ifndef GAME_LOADER_H
#define GAME_LOADER_H

#include <QAbstractListModel>
#include <QString>
#include <vector>
class IGameUiDelegate;
class QJsonObject;
class GameMetadata;

class GameLoader : public QAbstractListModel
{
	std::vector<GameMetadata> m_data;
	int m_gameCount;
	int m_typeCount;
	const QString invalid = "";
	const QString defaultType = "Miscellaneous";
	const QString defaultRules = "<p>Rules not given.</p><p>Figure it out yourself</p>";
public:
	GameLoader();
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

	bool loadFromDir(QString dir);
	bool load(QString file);
	bool add(IGameUiDelegate*, const QJsonObject&);

	int size() const;
	int nTypes() const;
	int nGames() const;
	bool valid(int index) const;
	GameMetadata* metadata(int index);
};

#endif // GAME_LOADER_H