#include <QMainWindow>

class GameUi;
class GameLoader;
class QListView;
class ImageManager;
class QGraphicsView;
class QSize;

class GameManager : public QMainWindow 
{
	Q_OBJECT
	GameUi *m_gameUi;
	GameLoader *m_loader;
	QListView *m_gameSelection;
	ImageManager *m_imageManager;
	QGraphicsView *m_gameView;
	void setupUi();
	void resizeEvent(QResizeEvent*);
public:
	GameManager(int width, int height, QWidget *parent=nullptr);
	// ~GameManager(); // GameManager exists for entire session. If implemented, pointers must be deleted
private slots:
	void setGame(const QModelIndex&);
};