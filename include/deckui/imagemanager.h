#ifndef IMAGE_MANAGER_H
#define IMAGE_MANAGER_H

#include "deck/card.h"
#include <QObject>
#include <vector>
#include <QFont>

#include <QGraphicsSimpleTextItem>
#include <QSvgRenderer>
#include <QPixmap>
#include <QPainter>
#include <QFontDatabase>
#include <QSize>

class ImageManager : public QObject
{
	Q_OBJECT
	QString m_style;
	QString m_dir;
	QString m_backgroundFile;
	double m_width;
	double m_height;
	double m_sceneWidth;
	double m_sceneHeight;
	QFont m_font;
	std::vector<QPixmap*> m_images;

	const QString defaultStyle = "default";
	const QString downCardName = "back";
	const QString defaultDir = ":deck";
	const QString fileName = "%1/%2/%3.svg";
	const QString defaultFontFile = ":FiraSans-Regular.ttf";
	const QString defaultBackgroundFile = ":background/default.png";
	const int minSceneWidth = 150; // shouldn't matter what game this is, already too small for human players
	const int minSceneHeight = 100;
	const double minLength=5;

	void setUpImages();
	void clearVecOfPtrs(std::vector<QPixmap *> &vec);
	bool loadFromDir(std::vector<QPixmap*> &images);
	void makeDummyImages(std::vector<QPixmap*> &images);
public:
	ImageManager(double sceneWidth, double sceneHeight);
	ImageManager(ImageManager const&)  = delete;
	void operator=(ImageManager const&) = delete;
	QPixmap* card(const Card *card=nullptr) const;
	QGraphicsSimpleTextItem* text(QString, qreal height);
	QBrush background();

	void setWidth(double);
	void setHeight(double);
	void setDir(QString);
	void setStyle(QString);
	void setBackground(QString);
	void setSceneSize(double, double);
	void setSceneSize(QSize);

	double width() const;
	double height() const;
	double sceneWidth() const;
	double sceneHeight() const;
	QString dir() const;
	QString style() const;
signals:
	void changedImage();
	void sceneResized(QSize);
};

#endif // IMAGE_MANAGER_H