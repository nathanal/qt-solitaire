#ifndef GAME_METADATA_H
#define GAME_METADATA_H

#include <QString>
class IGameUiDelegate;

class GameMetadata
{
	IGameUiDelegate *m_game;
	QString m_type;
	QString m_name;
	QString m_rules;
public:
	GameMetadata(IGameUiDelegate *, QString type, QString name, QString rules);
	GameMetadata(const GameMetadata&);
	~GameMetadata(){}
	IGameUiDelegate *game() const;
	QString type() const;
	QString name() const;
	QString rules() const;
	bool operator==(GameMetadata const&);
	bool operator<(GameMetadata const&);
	bool operator<=(GameMetadata const&);
	bool operator>(GameMetadata const&);
	bool operator>=(GameMetadata const&);
	friend void swap(GameMetadata&, GameMetadata&);
};

#endif // GAME_METADATA_H