#ifndef GAME_UI_H
#define GAME_UI_H

#include <QGraphicsScene>
#include <QTimer>
#include "deck/igame.h"
class QGraphicsSimpleTextItem;
class QGraphicsRectItem;
class ImageManager;
class IGame;
class IGameUiDelegate;
class CardUi;
class QSize;

class GameUi : public QGraphicsScene
{
	Q_OBJECT
	ImageManager *m_manager;
	IGameUiDelegate *m_delegate;
	IGame *m_game;

	std::vector<CardUi*> m_cards;
	bool m_paused;
	QTimer m_newGameDelay;
	const double m_defaultSpeed = 1.2;
	const int m_delay = 100;

	int moveAllToStock(); // return duration
	void organiseScene();
	int organisePile(IGame::Pile, int index);

	void prepForMove(CardUi* card, IGame::Pile pile, int pileIndex);
	void moveToPile(CardUi* card, IGame::Pile pile, int pileIndex);
	int moveToPile(CardUi* card, IGame::Pile pile, int pileIndex, double speed); // return duration

	void updateCardAppearance();
public:
	GameUi(ImageManager*, IGameUiDelegate*, QObject *parent=nullptr);
	virtual ~GameUi();

signals:
	void gameOver(bool isWon, int score);
public slots:
	bool resize(QSize);
	bool move(CardUi *from, IGame::Pile toPile, int toIndex);

	void pause();
	void unpause();

	void lost();
	void won();
	void newGame();
private slots:
	void dealFromStock();
};

#endif // GAME_UI_H