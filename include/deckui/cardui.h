#ifndef CARD_UI_H
#define CARD_UI_H

#include "deckui/imagemanager.h"
#include "deck/igame.h"

#include <QGraphicsObject>
#include <QPainter>
#include <QGraphicsDropShadowEffect>
#include <QTimeLine>

class CardUi : public QGraphicsObject
{
	Q_OBJECT
private:
	bool m_faceUp;
	Card *m_card;

	IGame::Pile m_pile;
	int m_index;

	ImageManager *m_manager;
	double m_width, m_height;

public:
	enum class Status { VALID_MOVE, INVALID_MOVE, NO_MOVE };
private:
	Status m_status;
	bool m_selected;
	QGraphicsDropShadowEffect *m_effect;

	const int transitionMax=100;
	const int transitionMin=0;
	const double nullSpeed = 0;
	QPointF m_moveEnd;
	double m_finalZValue; // used to change Z Value at the end of a move transition
	QPointF m_bufferDest;
	double m_bufferZValue;
	double m_bufferSpeed;
	bool m_bufferFlip;
	Card *m_bufferCard;
	QTimeLine *m_moveTransition;
public:
	CardUi(Card *card, ImageManager *manager, QGraphicsItem *parent = nullptr);
	CardUi(CardUi&); // besides for testing, unnecessary?

	bool isFaceUp() const;

	void setCard(Card*);
	void setStatus(Status, bool isSelected);
	IGame::Pile pile() const;
	int index() const;
	void setPile(IGame::Pile);
	void setIndex(int);

	QRectF boundingRect() const;
	void paint( QPainter*, const QStyleOptionGraphicsItem*, QWidget*);

private:
	void initAnimation();
	void updateEffects();
private slots:
	void moveByPercent(int);
	void finishMove();
	void delayedTranslate();
public slots:
	void translate(QPointF end, double finalZValue);
	int translate(QPointF end, double finalZValue, double speed); // return duration in ms
	void flip();
	void setFaceDown();
	void setFaceUp();
	void changeParent(QGraphicsItem *parent=nullptr);
	void updateImages();
};

#endif // CARD_UI_H