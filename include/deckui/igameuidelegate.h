#ifndef I_GAME_UI_DELEGATE_H
#define I_GAME_UI_DELEGATE_H

#include <QtPlugin>
#include <vector>
#include "deck/igame.h"
class QRectF;
class QPointF;
class QGraphicsItem;
class QGraphicsObject;
class CardUi;
class QObject;
class QEvent;
class ImageManager;
class GameUi;

class IGameUiDelegate
{
public:
	virtual ~IGameUiDelegate() {}
	virtual void setManager(ImageManager*) = 0;

	virtual std::vector<QGraphicsItem*> buildHeaders() = 0;
	virtual void updateHeaderPos(const double width, const double height) = 0;
	virtual void updateHeaderText() = 0;

	virtual void calculatePilePos(double width, double height) = 0;
	virtual QPointF calculatePos(const CardUi *) = 0;
	virtual QRectF pilePos(IGame::Pile) = 0;

	virtual IGame* game() = 0;
	virtual void setPaused(bool) = 0;
	virtual bool eventFilter(QObject *watched, QEvent *event) = 0;
	virtual void installFilter(QGraphicsObject *watched) = 0;
	virtual void buildConnections(GameUi*) = 0;
signals:
	virtual bool move(CardUi *from, IGame::Pile toPile, int toIndex) = 0;
};

#define IGameUiDelegate_iid "27.Jan.2019.Lee.Nathan.Solitaire.Project.IGameUiDelegate"

Q_DECLARE_INTERFACE(IGameUiDelegate, IGameUiDelegate_iid)

#endif // I_GAME_UI_DELEGATE_H