#ifndef I_GAME_H
#define I_GAME_H

#include "deck/card.h"
#include "deck/deck.h"

class IGame
{
public:
	enum class Pile { STOCK, WASTE, LAYOUT, CELLS, RESERVE, FOUNDATION };
		// Typically foundation and layout are 2D arrays, rest are 1D
protected:
	virtual void init() = 0;
	virtual void reset() = 0 ;
	virtual void updateScore(Card*) = 0;
public:
	IGame() {};
	virtual ~IGame() {};
	
	virtual void newGame(bool continueGame=false) = 0;
	virtual void newGame(int seed) = 0;
	virtual void restartGame() = 0;

	virtual bool load(QString) = 0;
	virtual QString save() const = 0;

	virtual bool move(Pile, Pile, int, int) = 0;
	
	virtual bool pileExists(Pile) const = 0;

	virtual int size() const = 0;
	virtual int pileInitSize(Pile) const = 0;
	virtual int pileSize(Pile) const = 0;

	virtual bool isCardThere(Pile, int) const = 0;
	virtual bool isCardVisible(Pile, int) const = 0;
	virtual bool isCardMovable(Pile, int) const = 0;

	virtual Card* card(Pile, int) const = 0;

	virtual int score() const = 0;
	virtual bool isWin() const = 0;
	virtual bool isGameOver() const = 0;
};

#endif // I_GAME_H