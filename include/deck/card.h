#ifndef CARD_H
#define CARD_H

#include <QString>

class Card
{
public:
	enum class Suit {INVALID=-1, DIAMONDS, CLUBS, HEARTS, SPADES};
	enum class Number {INVALID=-1, ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING};
	static const int cardsInDeck = 52; 
	static const int cardsInSuit = 13; 
	static const int suitsInDeck = 4;
	static const int invalid = -1;
private:
	int m_id;
	Suit m_suit;
	Number m_number;
	int m_value;
	void fillFromID();
public:
	Card(int i=Card::invalid);
	Card(Number, Suit); // terribly inefficient, don't use often
	Card& operator=(const Card&);

	bool isValid(int m_id) const;
	bool isValid() const;

	void swap(Card&);
	friend void swap(Card&, Card&);
	int id() const;
	Number number() const;
	int value() const;
	Suit suit() const;

	bool isRed() const;
	bool isBlack() const;
	bool isFaceCard() const;
	bool sameSuit(const Card&) const;
	bool sameColour(const Card&) const;
	bool sameNumber(const Card&) const;
	bool adjNumber(const Card&, bool wrap=false) const;
	bool adjSuit(const Card&, bool wrap=false) const;
	bool same(const Card&) const;
	bool operator==(const Card&) const;
	bool operator>(const Card&) const;
	bool operator>=(const Card&) const;
	bool operator<(const Card&) const;
	bool operator<=(const Card&) const;

	QString toString(Suit) const;
	QString toString(Number) const;
	QString toString(bool acronym=false) const;
	QString toUnicode(bool acronym=false) const;
};

#endif // CARD_H