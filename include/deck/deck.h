#ifndef DECK_H
#define DECK_H

#include "deck/card.h"
#include <algorithm>
#include <QByteArray>
#include <QRandomGenerator>
#include <QString>

class Deck
{
	QRandomGenerator rand;
	std::vector<Card> m_cards;
public:
	Deck(int nDecks=1);
	Deck &operator=(const Deck&);
	bool load(QString savedFile);
	QString save() const;

	void shuffle(bool continueGame=false);
	void shuffle(int seed);

	int size() const;
	Card& at(int index);
	const Card& at(int index) const;
	Card& operator[](int index);
	const Card& operator[](int index) const;

	static const char invalidBase64Char = '-'; // used for saving
	static Card invalidCard;
};

#endif