#!/bin/bash

main=default.svg
temp_dir=svg_file_buffer_TMP_93928
layer_ids=($(inkscape --query-all "$main" | grep layer | awk -F, '{print $1}'))

mkdir $temp_dir
for layer_id in "${layer_ids[@]}"
do
   :
  inkscape "$main" --export-id="$layer_id" --export-id-only --export-area-drawing --export-plain-svg="$temp_dir/$layer_id.svg"
  inkscape --file="$temp_dir/$layer_id.svg" --verb FileVacuum --verb FitCanvasToDrawing --verb FileSave --verb FileQuit
done

